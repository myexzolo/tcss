<!DOCTYPE html>
<html>

<?php include("inc/head.php"); ?>

<body class="hold-transition skin-black-light sidebar-mini sidebar-collapse">
<div class="wrapper">

<?php
  include("inc/header.php");
  include("inc/utils.php");
?>

  <!-- Left side column. contains the logo and sidebar -->
  <?php include("inc/sidebar.php"); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper bg">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <small>ข้อมูลอนุมัติเที่ยวรถ</small>
      </h1>

      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-home"></i> หน้าหลัก</a></li>
        <li>ข้อมูลรายวัน</li>
        <li class="active">ข้อมูลอนุมัติเที่ยวรถ</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
      <?php
          $date         = date('Y-m-d');
          $startDate    = date('Y-m-01', strtotime($date));
          $endDate      = date('Y-m-t', strtotime($date));

          $optionEmp            = getoptionEmp("","");
          $optionAffiliation    = getoptionAffiliation("1");

          $optionTrailerLicense  = getoptionTrailerLicense("");
          $optionCustomer  = getoptionCustomer("");
      ?>
      <div class="row">
      <!-- Main row -->
      <div class="col-md-12">
        <div class="panel panel-black">
          <div class="panel-heading">ค้นหาข้อมูลอนุมัติเที่ยวรถ</div>
            <div class="box-body">
              <div class="row" align="center">
                  <table>
                    <tr>
                      <td style="padding:5px;" align="right">จากวันที่: </td>
                      <td style="padding:5px;" align="left">
                        <input type="date" class="form-control" value="<?= $startDate ?>" id="startDate">
                        <input type="hidden" value="<?= $startDate ?>" id="startDateTmp">
                      </td>
                      <td style="padding:5px;width:150px;" align="right">ถึงวันที่ :</td>
                      <td style="padding:5px;" align="left">
                        <input type="date" class="form-control" value="<?= $endDate ?>" id="endDate">
                        <input type="hidden" value="<?= $endDate ?>" id="endDateTmp">
                      </td>
                      <td style="padding:5px;width:150px;" align="right">สังกัด :</td>
                      <td style="padding:5px;" align="left">
                        <select id="affiliationId" class="form-control select2" style="width: 100%;" required >
                          <option value="" >ทั้งหมด</option>
                          <?= $optionAffiliation ?>
                        </select>
                      </td>
                    </tr>
                    <tr>
                      <td style="padding:5px;" align="right">สถานะ :</td>
                      <td style="padding:5px;" align="left">
                        <select id="status" class="form-control select2" required >
                          <option value="1" >รอการอนุมัติ</option>
                          <option value="2" >อนุมัติเรียบร้อย</option>
                          <option value="3" >ยกเลิกสั่งงาน</option>
                          <option value="" >สถานะทั้งหมด</option>
                        </select>
                      </td>
                      <td style="padding:5px;" align="right">ชื่อพนักงานขับรถ :</td>
                      <td style="padding:5px;" align="left">
                        <select id="employeeId" class="form-control select2" style="width: 100%;" required >
                          <option value="" ></option>
                          <?= $optionEmp ?>
                        </select>
                      </td>
                      <td style="padding:5px;width:150px;" align="right">ทะเบียนรถ :</td>
                      <td style="padding:5px;" align="left">
                        <select id="trailer_id" class="form-control select2" style="width: 100%;" >
                          <option value="" ></option>
                          <?= $optionTrailerLicense ?>
                        </select>
                      </td>
                    </tr>
                    <tr>
                      <td style="padding:5px;width:150px;" align="right">ชื่อลูกค้า :</td>
                      <td colspan="2" style="padding:5px;" align="left">
                        <select id="cust_id" class="form-control select2" style="width: 100%;" required >
                          <option value="" >ทั้งหมด</option>
                          <?= $optionCustomer ?>
                        </select>
                      </td>
                      <td style="padding:5px;" align="right"></td>
                      <td style="padding:5px;" align="right"></td>
                      <td style="padding:5px;" align="right"></td>
                    </tr>
                    <tr>
                      <td style="padding:5px;padding-top:20px;" colspan="6" align="center">
                          <button type="button" id="btnSave" class="btn btn-primary btn-flat" onclick="search()" style="width:100px">ค้นหา</button>
                          <button type="button" onclick="reset()" class="btn btn-warning btn-flat" style="width:100px">ล้างค่า</button>
                      </td>
                    </tr>
                  </table>
                </div>
            </div>
        </div>
        <div class="panel panel-black">
          <div class="panel-heading">รายการข้อมูลอนุมัติเที่ยวรถ</div>
            <div class="box-body" >
              <form id="form-data" data-smk-icon="glyphicon-remove-sign" novalidate enctype="multipart/form-data">
              <div style="padding:5px" align="center">
                <table style="width:100%">
                  <td style="width:200px" align="left">
                    <input type="checkbox" id="checkApp" onclick="checkAll(this)"><label for="checkApp">&nbsp;เลือกทั้งหมด</label>
                  <td>
                  <td align="center">
                    <span class="fa fa-circle" style="color:green;font-size:20px;"></span>&nbsp;รถบริษัท&nbsp;
                    <span class="fa fa-circle" style="color:red;font-size:20px;"></span>&nbsp;รถร่วม
                  <td>
                  <td style="width:200px"><td>
                </table>
              </div>
                <div style="width:100%;">
                  <div id="show-page" >
                    <div class="overlay">Loading.... <i class="fa fa-circle-o-notch fa-spin"></i></div>
                  </div>
                </div>
                <div style="margin-top:5px;">
                  <button type="submit" class="btn btn-primary pull-right btn-flat"  style="width:100px">อนุมัติ</button>
                </div>
              </form>
            </div>


      <!--  # coding -->
      </div>

      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <!-- Modal -->

<?php include("inc/foot.php"); ?>

</div>
<!-- ./wrapper -->

<?php include("inc/footer.php"); ?>
<script src="js/approve.js" type="text/javascript"></script>
<script>
  $(document).ready(function() {
    search();
  });
</script>
</body>
</html>
