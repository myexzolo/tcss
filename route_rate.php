<!DOCTYPE html>
<html>

<?php include("inc/head.php"); ?>

<body class="hold-transition skin-black-light sidebar-mini sidebar-collapse">
<div class="wrapper">

<?php include("inc/header.php"); ?>

  <!-- Left side column. contains the logo and sidebar -->
  <?php
  include("inc/sidebar.php");
  $route_id = $_GET['c'];
  ?>
 <style>
 td{
   padding:5px;
 }
</style>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper bg">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>

        <small>แก้ไขค่าใช้จ่ายตาม Rate เส้นทาง</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-home"></i> หน้าหลัก</a></li>
        <li><a href="route.php">กำหนดค่าใช้จ่ายตามเส้นทาง</a></li>
        <li class="active">แก้ไขค่าใช้จ่ายตาม Rate เส้นทาง</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
      <div class="row">
      <!-- Main row -->
      <div class="col-md-12">
        <div class="panel panel-black" >
          <div class="panel-heading">แก้ไขค่าใช้จ่ายตาม Rate เส้นทาง</div>
          <form id="form-data" data-smk-icon="glyphicon-remove-sign" novalidate >
          <!-- <form id="form-data" data-smk-icon="glyphicon-remove-sign" novalidate  method="post" action="ajax/route/manage_rate.php"> -->
            <input value="<?= $route_id ?>" id="route_id" type="hidden">
            <div class="box-body">
              <div id="show-page" style="overflow-x:auto">
                <div class="overlay">Loading.... <i class="fa fa-circle-o-notch fa-spin"></i></div>
              </div>
          </div>
        </form>
        </div>
      </div>
      <!--  # coding -->
      </div>
      <!-- /.row -->
    <!-- /.content -->
  <!-- /.content-wrapper -->
</section>
  <!-- Modal -->
</div>
<?php include("inc/foot.php"); ?>

</div>
<!-- ./wrapper -->
<?php include("inc/footer.php"); ?>
<script src="js/route_rate.js" type="text/javascript"></script>
</body>
</html>
