      <div class="row" id="headMain">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box" style="background-color: #eeff00;color: #030303;">
            <div class="inner">
              <h3><div id="checkInNum">0</div></h3>
              <p>Member CheckIn</p>
            </div>
            <div class="icon">
              <i class="ion ion-ios-clock" style="color:#030303;"></i>
            </div>
            <a href="#" class="small-box-footer" style="padding:6px;color: #030303;">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box " style="background-color: #333333;color: #eeff00;">
            <div class="inner">
              <h3><div id="memberNum">0</div></h3>
              <p >Member</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-stalker" style="color:#eeff00;"></i>
            </div>
            <a href="member.php" class="small-box-footer" style="padding:6px;color: #eeff00;">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <!-- fix for small devices only -->
        <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="box boxBlack">
              <div class="box-header with-border">
                <p class="box-title">Quick Menu</p>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>  </button>
                </div>
              </div>
              <div class="box-body" align="center">
                  <a class="btn btn-app" style="background-color: #333333;color: #eeff00;" href='pos.php'>
                    <i class="fa fa-calculator"></i> POS
                  </a>
                  <a class="btn btn-app" style="background-color: #333333;color: #eeff00;" href='member.php'>
                  <i class="fa fa-users"></i> Member
                </a>
                <a class="btn btn-app" style="background-color: #333333;color: #eeff00;" onclick="notifyMe('','')">
                  <i class="fa fa-edit"></i> Edit
                </a>
                <a class="btn btn-app" style="background-color: #333333;color: #eeff00;">
                  <i class="fa fa-edit"></i> Edit
                </a>
            </div>
         </div>
        </div>
        <!-- /.col -->
      </div>

      <script src="js/head.js"></script>
