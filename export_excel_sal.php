<?php
include('conf/connect.php');
include('inc/utils.php');


$startDate      = isset($_GET['startDate'])?$_GET['startDate']:"";
$endDate        = isset($_GET['endDate'])?$_GET['endDate']:"";
$employeeId     = isset($_GET['employeeId'])?$_GET['employeeId']:"";
$strExcelFileName="export_sal.xls";

header("Content-Type: application/x-msexcel; name=\"$strExcelFileName\"");
header("Content-Disposition: inline; filename=\"$strExcelFileName\"");
header("Pragma:no-cache");


$fuel_litres = 0;
$fuel_costs = 0;
$blank_charges = 0;
$allowances = 0;
$kog_expenses = 0;
$job_ended_acc_expenses = 0;
$allowance_oths = 0;
$job_ended_expressways = 0;
$job_ended_recaps = 0;
$total_amount_allowances = 0;
$total_amount_receives = 0;
$withholdings = 0;
$job_order_profits = 0;
$fuel_driver_bills =  0;


$numRow = 0;
$con = "";

$iNum = 0;

$employeeIdTmp = "";

if($employeeId != "")
{
  $con .= " and jo.employee_id ='". $employeeId ."'";
}

?>
<html xmlns:o="urn:schemas-microsoft-com:office:office"xmlns:x="urn:schemas-microsoft-com:office:excel"xmlns="http://www.w3.org/TR/REC-html40">

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
<div id="SiXhEaD_Excel" align=center x:publishsource="Excel">
  <div align="center" style="font-size:14px;"><b>รายงานรายได้พนักงานขับรถ</b></div>
  <div align="center" style="font-size:14px;">
    <b>
      ประจำวันที่ <?= formatDateTh($startDate) ?> ถึงวันที่ <?= formatDateTh($endDate) ?>
    </b>
  </div>
  <?php
  $con .= " and jo.job_order_date between '". $startDate ."' and '". $endDate ."'";
  //$sql = "SELECT * FROM tb_po_customer pc, tb_customer_master cm, tb_employee_master em where pc.Cust_ID = cm.Cust_ID and pc.Employee_No = em.Employee_No $con ";
  $sql = "SELECT jo.*,em.employee_name,t.license_plate
  FROM tb_job_order jo, tb_employee_master em, tb_trailer t
  where jo.employee_id = em.employee_id $con and  jo.trailer_id = t.trailer_id and  jo.job_status_id = '2' and invoice_code is not null
  order by jo.employee_id,jo.job_order_date";
  //echo $sql;
  $query  = mysqli_query($conn,$sql);
  $num = mysqli_num_rows($query);

  $totalTransfer = 0;
  for ($i=1; $i <= $num ; $i++) {
    $row = mysqli_fetch_assoc($query);
    $Employee_Name      = $row['employee_name'];
    $license_plate      = $row['license_plate'];
    $id                 = $row['id'];
    $job_order_no       = $row['job_order_no'];
    $delivery_type_id   = $row['delivery_type_id'];//รูปแบบการขนส่ง
    $job_order_date     = formatDate($row['job_order_date'],'d/m/Y');//วันที่ออกใบสั่งงาน
    $job_delivery_date  = formatDate($row['job_delivery_date'],'d/m/Y');;//วันที่กำหนดส่งสินค้า
    $cust_id            = $row['cust_id'];//รหัสลูกค้า
    $employee_id        = $row['employee_id'];//รหัสพนักงาน
    $trailer_id         = $row['trailer_id'];//หัว หมายเลขทะเบียนรถ
    $away_regi_no       = $row['away_regi_no'];//หาง
    $away_spec_no       = $row['away_spec_no'];//สเปคหาง
    $affiliation_id     = $row['affiliation_id'];//รหัส รถ(สังกัด)
    $route_id           = $row['route_id'];//รหัสเส้นทาง
    $source             = $row['source'];//ต้นทาง
    $destination        = $row['destination'];//ปลายทาง
    $distance           = $row['distance'];//ระยะทาง
    $cust_dp            = $row['cust_dp'];//DP
    $delivery_doc       = $row['delivery_doc'];//ใบส่งของ
    $department         = $row['department'];//หน่วยงาน
    $product_name       = $row['product_name'];//สินค้า
    $number_of_trips    = $row['number_of_trips'];//จำนวนเที่ยว
    $weights            = $row['weights'];//น้ำหนัก(ตัน)
    $shipping_amount    = $row['shipping_amount'];//ราคาค่าขนส่ง
    $date_product_in    = $row['date_product_in'];//วันทีรับสินค้า
    $time_product_in    = $row['time_product_in'];//เวลารับสินค้า
    $date_product_out   = $row['date_product_out'];//วันที่ออกสินค้า
    $time_product_out   = $row['time_product_out'];//เวลาออกสินค้า
    $date_delivery_in   = $row['date_delivery_in'];//วันทีส่งสินค้า
    $time_delivery_in   = $row['time_delivery_in'];//เวลาเข้าส่งสินค้า
    $date_delivery_out  = $row['date_delivery_out'];//วันที่ออกส่งสินค้า
    $time_delivery_out  = $row['time_delivery_out'];//เวลาออกส่งสินค้า
    $fuel_cost          = $row['fuel_cost'];//ค่าน้ำมัน
    $fuel_litre         = $row['fuel_litre'];//จำนวนลิตร
    $blank_charge       = $row['blank_charge'];//ค่าตีเปล่า
    $allowance          = $row['allowance'];//เบี้ยเลี้ยง
    $kog_expense        = $row['kog_expense'];//หางคอก
    $allowance_oth      = $row['allowance_oth'];//เบี้ยงเลี้ยงอื่นๆ
    $remark             = $row['remark'];
    $shipment           = $row['shipment'];
    $price_type_id      = $row['price_type_id'];//รหัสการคำนวณ
    $one_trip_ton       = $row['one_trip_ton'];//ราคาต่อตัน
    $ext_one_trip_ton   = $row['ext_one_trip_ton'];//ราคารถร่วมต่อตัน
    $price_per_trip     = $row['price_per_trip'];//ราคาต่อเที่ยว
    $ext_price_per_trip = $row['ext_price_per_trip'];//ราคารถร่วมต่อเที่ยว
    $total_amount_receive = $row['total_amount_receive'];//ราคาค่าขนส่ง
    $total_amount_ap_pay  = $row['total_amount_ap_pay'];//ราคาจ่ายรถร่วม
    $total_amount_allowance   = $row['total_amount_allowance'];//รวมค่าใช้จ่าย
    $job_ended_clearance      = $row['job_ended_clearance'];//ค่าเคลียร์ค่าปรับ
    $job_ended_recap          = $row['job_ended_recap'];//ค่าปะยาง
    $job_ended_expressway     = $row['job_ended_expressway'];//ค่าทางด่วน
    $job_ended_passage_fee    = $row['job_ended_passage_fee'];//ค่าธรรมเนียมผ่านท่าเรือ
    $job_ended_repaires       = $row['job_ended_repaires'];//ค่าซ่อม
    $job_ended_acc_expense    = $row['job_ended_acc_expense'];//ค่าทำบัญชี
    $job_ended_other_expense  = $row['job_ended_other_expense'];//คชจ.อื่นๆ
    $fuel_driver_bill         = chkNum($row['fuel_driver_bill']);//บิลน้ำมันจากคนขับ
    $job_status_id            = $row['job_status_id'];
    $job_order_profit         = $row['job_order_profit'];

    $invoice_code             = $row['invoice_code'];
    $period_code              = $row['period_code'];
    $license_plate            = $row['license_plate'];


    $transfer = ($fuel_cost + $allowance + $kog_expense + $blank_charge + $allowance_oth);
    $totalTransfer +=  $transfer;

    $withholding  =  ($total_amount_receive * 0.01);

    $job_order_profit -= $withholding;

   //echo  $employee_id." <> ".$employeeIdTmp."<br>";
   if($employee_id != $employeeIdTmp){
     if($employeeIdTmp != ""){
     ?>
       <tr>
         <td colspan="8" class="text-right " ><b>รวม</b></td>
         <td class="text-right " ><b><?= number_format($fuel_litres,2); ?></b></td>
         <td class="text-right " ><b><?= number_format($fuel_costs,2);  ?></b></td>
         <td class="text-right " ><b><?= number_format($blank_charges,2); ?></b></td>
         <td class="text-right " ><b><?= number_format($allowances,2); ?></b></td>
         <td class="text-right " ><b><?= number_format($kog_expenses,2); ?></b></td>
         <td class="text-right " ><b><?= number_format($job_ended_acc_expenses,2); ?></b></td>
         <td class="text-right " ><b><?= number_format($allowance_oths,2); ?></b></td>
         <td class="text-right " ><b><?= number_format($job_ended_expressways,2); ?></b></td>
         <td class="text-right " ><b><?= number_format($job_ended_recaps,2); ?></b></td>
         <td class="text-right " ><b><?= number_format($total_amount_allowances,2); ?></b></td>
         <td class="text-right " ><b><?= number_format($total_amount_receives,2); ?></b></td>
         <td class="text-right " ><b><?= number_format($withholdings,2); ?></b></td>
         <td class="text-right " ><b><?= number_format($job_order_profits,2); ?></b></td>
         <td class="text-right " ><b><?= number_format($fuel_driver_bills,2); ?></b></td>
       </tr>
       </tbody>
       </table>
     </div>
   <?php
           $fuel_litres            = 0;
           $fuel_costs             = 0;
           $blank_charges          = 0;
           $allowances             = 0;
           $kog_expenses           = 0;
           $job_ended_acc_expenses = 0;
           $allowance_oths         = 0;
           $job_ended_expressways  = 0;
           $job_ended_recaps       = 0;
           $total_amount_allowances  = 0;
           $total_amount_receives  = 0;
           $withholdings           = 0;
           $job_order_profits      = 0;
           $fuel_driver_bills      = 0;

         }

         $employeeIdTmp = $employee_id;
         $iNum = 0;

         //echo  ">>>>".$employee_id." <<<<>>>> ".$employeeIdTmp."<br>";

   ?>
  <div style="font-size:14px;" align="left"><b><?= $Employee_Name ?></b></div>
  <table x:str border=1 cellpadding=0 cellspacing=1 width=100% style="border-collapse:collapse">
      <thead>
        <tr>
    			<th style="width:30px;" >No.</th>
          <th style="width:90px;" >วันที่สั่งจ้าง</th>
          <th style="width:98px;" >เลขที่เอกสาร</th>
          <th style="width:100px;" >Shipment</th>
          <th style="width:80px;" >ทะเบียน</th>
          <th style="width:115px;" >เลขวางบิล</th>
    			<th style="width:180px;" >ต้นทาง</th>
    			<th >ปลายทาง</th>
    			<th  style="width:80px;">จำนวนลิตร</th>
    			<th >ค่าน้ำมัน</th>
          <th >ตีเปล่า</th>
          <th >เบี้ยเลี้ยง</th>
          <th >คอก</th>
          <th  style="width:50px;">บัญชี</th>
          <th  >อื่นๆ</th>
          <th  >ทางด่วน</th>
          <th  style="width:80px;">ปะยาง</th>
          <th  style="width:85px;">รวมค่าใช้จ่าย</th>
          <th  style="width:100px;">รายได้ค่าขนส่ง</th>
          <th  style="width:60px;">หัก 1%</th>
          <th  style="width:80px;">กำไรขั้นต้น</th>
          <th  style="width:80px;">บิลน้ำมัน</th>
    		</tr>
      </thead>
      <tbody>
    <?php
    }
    $fuel_litres            += $fuel_litre;
    $fuel_costs             += $fuel_cost;
    $blank_charges          += $blank_charge;
    $allowances             += $allowance;
    $kog_expenses           += $kog_expense;
    $job_ended_acc_expenses += $job_ended_acc_expense;
    $allowance_oths         += $allowance_oth;
    $job_ended_expressways  += $job_ended_expressway;
    $job_ended_recaps       += $job_ended_recap;
    $total_amount_allowances  += $total_amount_allowance;
    $total_amount_receives  += $total_amount_receive;
    $withholdings           += $withholding;
    $job_order_profits      += $job_order_profit;
    $fuel_driver_bills      += $fuel_driver_bill;
    $iNum++;
    ?>
      <tr>
        <td class="text-center " ><?= $iNum ?></td>
        <td class="text-center " ><?= $job_order_date ?></td>
        <td class="text-left " ><?= $job_order_no ?></td>
        <td class="text-center " ><?= $shipment ?></td>
        <td class="text-center " ><?= $license_plate ?></td>
        <td class="text-center " ><?= $invoice_code ?></td>
        <td class="text-left " ><?= $source ?></td>
        <td class="text-left " ><?= $destination ?></td>
        <td class="text-right " ><?= number_format($fuel_litre,2); ?></td>
        <td class="text-right " ><?= number_format($fuel_cost,2);  ?></td>
        <td class="text-right " ><?= number_format($blank_charge,2); ?></td>
        <td class="text-right " ><?= number_format($allowance,2); ?></td>
        <td class="text-right " ><?= number_format($kog_expense,2); ?></td>
        <td class="text-right " ><?= number_format($job_ended_acc_expense,2); ?></td>
        <td class="text-right " ><?= number_format($allowance_oth,2); ?></td>
        <td class="text-right " ><?= number_format($job_ended_expressway,2); ?></td>
        <td class="text-right " ><?= number_format($job_ended_recap,2); ?></td>
        <td class="text-right " ><?= number_format($total_amount_allowance,2); ?></td>
        <td class="text-right " ><?= number_format($total_amount_receive,2); ?></td>
        <td class="text-right " ><?= number_format($withholding,2); ?></td>
        <td class="text-right " ><?= number_format($job_order_profit,2); ?></td>
        <td class="text-right " ><?= number_format($fuel_driver_bill,2); ?></td>

      </tr>
    <?php
    }

    if($num == 0){
      ?>
      <br>
      <div class="divo" style="width:100%;overflow-x: auto">
      <table x:str border=1 cellpadding=0 cellspacing=1 width=100% style="border-collapse:collapse">
        <thead>
          <tr>
      			<th  style="width:30px;">No.</th>
            <th  style="width:90px;">วันที่สั่งจ้าง</th>
            <th  style="width:98px;">เลขที่เอกสาร</th>
            <th  style="width:100px;">Shipment</th>
            <th  style="width:80px;">ทะเบียน</th>
            <th  style="width:115px;">เลขวางบิล</th>
      			<th  style="width:180px;">ต้นทาง</th>
      			<th  >ปลายทาง</th>
      			<th  style="width:80px;">จำนวนลิตร</th>
      			<th  >ค่าน้ำมัน</th>
            <th  >ตีเปล่า</th>
            <th  >เบี้ยเลี้ยง</th>
            <th  >คอก</th>
            <th  style="width:50px;">บัญชี</th>
            <th  >อื่นๆ</th>
            <th  >ทางด่วน</th>
            <th  style="width:80px;">ปะยาง</th>
            <th  style="width:85px;">รวมค่าใช้จ่าย</th>
            <th  style="width:100px;">รายได้ค่าขนส่ง</th>
            <th  style="width:60px;">หัก 1%</th>
            <th  style="width:80px;">กำไรขั้นต้น</th>
            <th  style="width:80px;">บิลน้ำมัน</th>
      		</tr>
        </thead>
        <tbody>
      <tr>
        <td colspan="22" class="text-center" ><div>ไม่พบข้อมูล</div></td>
      </tr>
    </tbody>
    </table>
    </div>
      <?php
    }else{
      ?>
      <tr>
        <td colspan="8" class="text-right " ><b>รวม</b></td>
        <td class="text-right " ><b><?= number_format($fuel_litres,2); ?></b></td>
        <td class="text-right " ><b><?= number_format($fuel_costs,2);  ?></b></td>
        <td class="text-right " ><b><?= number_format($blank_charges,2); ?></b></td>
        <td class="text-right " ><b><?= number_format($allowances,2); ?></b></td>
        <td class="text-right " ><b><?= number_format($kog_expenses,2); ?></b></td>
        <td class="text-right " ><b><?= number_format($job_ended_acc_expenses,2); ?></b></td>
        <td class="text-right " ><b><?= number_format($allowance_oths,2); ?></b></td>
        <td class="text-right " ><b><?= number_format($job_ended_expressways,2); ?></b></td>
        <td class="text-right " ><b><?= number_format($job_ended_recaps,2); ?></b></td>
        <td class="text-right " ><b><?= number_format($total_amount_allowances,2); ?></b></td>
        <td class="text-right " ><b><?= number_format($total_amount_receives,2); ?></b></td>
        <td class="text-right " ><b><?= number_format($withholdings,2); ?></b></td>
        <td class="text-right " ><b><?= number_format($job_order_profits,2); ?></b></td>
        <td class="text-right " ><b><?= number_format($fuel_driver_bills,2); ?></b></td>
      </tr>
      </tbody>
      </table>
    </div>
      <?php

    }
    //}
    ?>

    </tbody>
    </table>
<script>
window.onbeforeunload = function(){return false;};
setTimeout(function(){window.close();}, 10000);
</script>
</body>
</html>
