<!DOCTYPE html>
<html>

<?php include("inc/head.php"); ?>

<body class="hold-transition skin-black-light sidebar-mini sidebar-collapse">
<div class="wrapper">

<?php

  include("inc/header.php");
  include("inc/sidebar.php");
  include("inc/utils.php");

?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper bg">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <small>บันทึกข้อมูลใบสั่งซื้อ (PO)</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-home"></i> หน้าหลัก</a></li>
        <li>ข้อมูลรายวัน</li>
        <li><a href="po_list.php">ข้อมูลใบสั่งซื้อ</a></li>
        <li class="active">บันทึกข้อมูลใบสั่งซื้อ</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->

      <?php
          $dateNow      = date('Y-m-d');

          $ID  = isset($_POST['id'])?$_POST['id']:"";

          $Po_No              = "";
          $job_type_id        = "";
          $Date_Po            = "";
          $Date_Send_Delivery = "";
          $Employee_No        = "";
          $Cust_ID            = "";
          $source             = "";
          $destination        = "";
          $Cust_DP            = "";
          $Department_of_Location_Delivery = "";
          $Shipment_doc       = "";
          $trailer_no         = "";
          $away_regi_no       = "";
          $away_spec_no       = "";
          $Product_Name       = "";
          $Number_of_trips    = "";
          $Weights            = "";
          $Shipping_Amount    = "";
          $Date_GetProduct_In     = "";
          $Get_Product_time_IN    = "";
          $Date_GetProduct_Out    = "";
          $Get_Product_time_OUT   = "";
          $Date_DeliveryProduct_In  = "";
          $DeliveryProduct_Time_IN  = "";
          $Date_DeliveryProduct_Out = "";
          $DeliveryProduct_Time_OUT = "";
          $Fuel_Cost          = "";
          $fuel_Litre         = "";
          $Blank_Charge       = "";
          $Remark             = "";

          $optionCustomer     = "";
          $optionEmp          = "";
          $optionTrailer      = "";
          $po_status_id       = "";

          if($ID == ""){

            $d        = date('Y-m-d H:i:s');
            $strDay   = date('d',strtotime($d));
            $strMonth = date('m',strtotime($d));
            $strYear  = substr(date("Y",strtotime($d))+543,2,2);

            $Code = "TP".$strYear.$strMonth.$strDay;

            $po_status_id = "1";

            //echo $Code;

            $sql = "SELECT max(Po_No) as Po_No FROM tb_po_customer where Po_No LIKE 'TP%'";
            $query  = mysqli_query($conn,$sql);
            $row    = mysqli_fetch_assoc($query);
            $Po_No  = $row['Po_No'];

            if(!empty($memberCode)){
              $lastNum = substr($Po_No,strlen($Code));
              $lastNum = $lastNum + 1;
              $Po_No = $Code.sprintf("%03d", $lastNum);
            }else{
              $Po_No = $Code.sprintf("%03d", 1);
            }
          }else{

            $sql = "SELECT * FROM tb_po_customer where 	ID ='$ID'";
            $query  = mysqli_query($conn,$sql);
            $row    = mysqli_fetch_assoc($query);

            $Po_No              = $row['Po_No'];
            $job_type_id        = $row['job_type_id'];
            $Date_Po            = $row['Date_Po'];
            $Date_Send_Delivery = $row['Date_Send_Delivery'];
            $Employee_No        = $row['Employee_No'];
            $Cust_ID            = $row['Cust_ID'];
            $source             = $row['source'];
            $destination        = $row['destination'];
            $Cust_DP            = $row['Cust_DP'];
            $Department_of_Location_Delivery = $row['Department_of_Location_Delivery'];
            $Shipment_doc       = $row['Shipment_doc'];
            $trailer_no         = $row['trailer_no'];
            $away_regi_no       = $row['away_regi_no'];
            $away_spec_no       = $row['away_spec_no'];
            $Product_Name       = $row['Product_Name'];
            $Number_of_trips    = $row['Number_of_Trips'];
            $weight             = $row['Weights'];
            $Shipping_Amount    = $row['Shipping_Amount'];
            $Date_GetProduct_In     = $row['Date_GetProduct_In'];
            $Get_Product_time_IN    = $row['GetProduct_Time_In'];
            $Date_GetProduct_Out    = $row['Date_GetProduct_Out'];
            $Get_Product_time_OUT   = $row['GetProduct_Time_Out'];
            $Date_DeliveryProduct_In  = $row['Date_DeliveryProduct_In'];
            $DeliveryProduct_Time_IN  = $row['DeliveryProduct_Time_In'];
            $Date_DeliveryProduct_Out = $row['Date_DeliveryProduct_Out'];
            $DeliveryProduct_Time_OUT = $row['DeliveryProduct_Time_Out'];
            $Fuel_Cost          = $row['Fuel_Cost'];
            $fuel_Litre         = $row['Fuel_Litre'];
            $Blank_Charge       = $row['Blank_Charge'];
            $Remark             = $row['Remark'];
            $po_status_id       = $row['po_status_id'];

          }

          if($Date_Po == ""){
            $Date_Po = $dateNow;
          }

          $optionEmp      = getoptionEmp($Employee_No);
          $optionCustomer = getoptionCustomer($Cust_ID);
          $optionTrailer  = getoptionTrailerNo($trailer_no);

      ?>
      <form id="form-data" data-smk-icon="glyphicon-remove-sign" novalidate enctype="multipart/form-data">
      <div class="row">
      <!-- Main row -->
      <div class="col-md-12">
        <div class="panel panel-black">
          <div class="panel-heading">ข้อมูลใบสั่งซื้อ (PO)</div>
            <div class="box-body" >
                <div id="show-page">
                  <div class="row">
                    <div class="col-md-2">
                      <div class="form-group">
                        <label>เลขที่ PO</label>
                        <input value="<?= $Po_No ?>" name="Po_No" type="text" class="form-control" placeholder="No PO" required readonly>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label>วันที่</label>
                        <input value="<?= $Date_Po ?>" name="Date_Po" type="date" class="form-control" placeholder="" >
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label>กำหนดส่ง</label>
                        <input value="<?= $Date_Send_Delivery ?>" name="Date_Send_Delivery" type="date" class="form-control" placeholder="" >
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>ชื่อพนักงาน</label>
                        <select name="Employee_No"  id="Employee_No" class="form-control select2" style="width: 100%;" required >
                          <option value="" >เลือกพนักงานขับรถ</option>
                          <?= $optionEmp ?>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>ลูกค้า</label>
                        <select name="Cust_ID"  id="Cust_ID" class="form-control select2" style="width: 100%;" required>
                          <option value="" >เลือกลูกค้า</option>
                          <?= $optionCustomer ?>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>สถานที่รับ</label>
                        <input value="<?= $source ?>" name="source" type="text" class="form-control" placeholder="สถานที่รับ" >
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>สถานที่ส่ง</label>
                        <input value="<?=$destination ?>" name="destination" type="text" class="form-control" placeholder="สถานที่ส่ง" >
                      </div>
                    </div>
                  </div>
                    <div class="row">
                      <div class="col-md-5">
                        <div class="form-group">
                          <label>DP</label>
                          <textarea class="form-control" name="Cust_DP" id="Cust_DP" rows="3" placeholder="DP ..." required><?= $Cust_DP ?></textarea>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label>ใบส่งของ</label>
                          <input value="<?= $Shipment_doc ?>" name="Shipment_doc" type="text" class="form-control" placeholder="ใบส่งของ" >
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label>หน่วยงาน</label>
                          <input value="<?= $Department_of_Location_Delivery ?>" name="Department_of_Location_Delivery" type="text" class="form-control" placeholder="หน่วยงาน" >
                        </div>
                      </div>
                    </div>
                    <div class="row">
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>หัว</label>
                        <select name="trailer_no"  id="trailer_no" class="form-control select2" style="width: 100%;" required>
                          <option value="" >หมายเลขทะเบียนรถ</option>
                          <?= $optionTrailer ?>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>หาง</label>
                        <input value="<?=$away_regi_no ?>" name="away_regi_no" type="text" class="form-control" placeholder="หาง" >
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Spec หาง</label>
                        <input value="<?=$away_spec_no ?>" name="away_spec_no" type="text" class="form-control" placeholder="Spac" >
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                        <div class="col-md-12 text-center" style="background-color:#00ffff;height:30px;line-height:30px;margin-top:5px;margin-bottom:5px;">
                          <b>รายการที่สั่งจ้าง</b>
                        </div>
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>สินค้า</label>
                              <input value="<?=$Product_Name ?>" name="Product_Name" type="text" class="form-control" placeholder="สินค้า" >
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>จำนวนเที่ยว</label>
                              <input value="<?=$Number_of_trips ?>" name="Number_of_trips" type="text" class="form-control" placeholder="จำนวนเที่ยว" >
                            </div>
                          </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="col-md-12 text-center" style="background-color:#00ffff;height:30px;line-height:30px;margin-top:5px;margin-bottom:5px;">
                          <b>ขนส่งแบบเที่ยว</b>
                        </div>
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>น้ำหนัก(ตัน)</label>
                              <input value="<?=$Weights ?>" name="Weights" type="text" class="form-control" placeholder="น้ำหนัก" >
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>ราคาค่าขนส่ง</label>
                              <input value="<?=$Shipping_Amount ?>" name="Shipping_Amount" type="text" class="form-control" placeholder="ราคาค่าขนส่ง" >
                            </div>
                          </div>
                        </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>วันทีรับสินค้า</label>
                        <input value="<?= $Date_GetProduct_In ?>" name="Date_GetProduct_In" type="date" class="form-control" placeholder="" >
                      </div>
                    </div>
                    <div class="col-md-2">
                      <div class="form-group">
                        <label>เวลาเข้า</label>
                        <input value="<?= $Get_Product_time_IN ?>" name="Get_Product_time_IN" type="time" class="form-control" placeholder="" >
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>วันที่ออก</label>
                        <input value="<?= $Date_GetProduct_Out ?>" name="Date_GetProduct_Out" type="date" class="form-control" placeholder="" >
                      </div>
                    </div>
                    <div class="col-md-2">
                      <div class="form-group">
                        <label>เวลาออก</label>
                        <input value="<?= $Get_Product_time_OUT ?>" name="Get_Product_time_OUT" type="time" class="form-control" placeholder="" >
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>วันที่ส่งสินค้า</label>
                        <input value="<?= $Date_DeliveryProduct_In ?>" name="Date_DeliveryProduct_In" type="date" class="form-control" placeholder="" >
                      </div>
                    </div>
                    <div class="col-md-2">
                      <div class="form-group">
                        <label>เวลาเข้า</label>
                        <input value="<?= $DeliveryProduct_Time_IN ?>" name="DeliveryProduct_Time_IN" type="time" class="form-control" placeholder="" >
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>วันที่ออก</label>
                        <input value="<?= $Date_DeliveryProduct_Out ?>" name="Date_DeliveryProduct_Out" type="date" class="form-control" placeholder="" >
                      </div>
                    </div>
                    <div class="col-md-2">
                      <div class="form-group">
                        <label>เวลาออก</label>
                        <input value="<?= $DeliveryProduct_Time_OUT ?>" name="DeliveryProduct_Time_OUT" type="time" class="form-control" placeholder="" >
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="text-center" style="background-color:#00ffff;height:30px;line-height:30px;margin-top:5px;margin-bottom:5px;">
                        <b>ค่าใช้จ่าย</b>
                      </div>
                  </div>
                  <div class="col-md-2">
                    <div class="form-group">
                      <label>ค่าน้ำมัน</label>
                      <input value="<?= $Fuel_Cost ?>" name="Fuel_Cost" type="text" class="form-control" placeholder="" >
                    </div>
                  </div>
                  <div class="col-md-2">
                    <div class="form-group">
                      <label>จำนวนลิตร</label>
                      <input value="<?= $fuel_Litre ?>" name="fuel_Litre" type="text" class="form-control" placeholder="" >
                    </div>
                  </div>
                  <div class="col-md-2">
                    <div class="form-group">
                      <label>ค่าตีเปล่า</label>
                      <input value="<?= $Blank_Charge ?>" name="Blank_Charge" type="text" class="form-control" placeholder="" >
                    </div>
                  </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>หมายเหตุ</label>
                        <textarea class="form-control" name="Remark" id="Remark" rows="3" placeholder="Remark ..." ><?= $Remark ?></textarea>
                      </div>
                    </div>
                  </div>
                  <input  type="hidden" value="<?= $ID ?>" name="ID" type="text" >
                  <input  type="hidden" value="1" name="job_type_id" type="text" >
                  <input  type="hidden" value="<?= $po_status_id ?>" name="po_status_id" type="text" >
                </div>
                <div align="center">
                  <button type="submit" class="btn btn-primary btn-flat " style="width:100px">
                    <i class="fa fa-save"></i> บันทึก
                  </button>
                  <button type="reset" class="btn btn-warning btn-flat " style="width:100px">
                    <i class="glyphicon glyphicon-remove"></i> ล้างค่า
                  </button>
                  <button type="button" onclick="gotoPage('po_list.php')" class="btn btn-success btn-flat " style="width:100px">
                    <i class="fa fa-mail-reply"></i> ย้อนกลับ
                  </button>
                </div>
            </div>
      <!--  # coding -->
      </div>
    </div>
    </div>
    </form>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <!-- Modal -->

<?php include("inc/foot.php"); ?>

</div>
<!-- ./wrapper -->

<?php include("inc/footer.php"); ?>
<script src="js/po.js" type="text/javascript"></script>
</body>
</html>
