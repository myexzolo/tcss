
  function show(){
    var source = $('#s_source').val();
    var destination = $('#s_destination').val();
    $.post("ajax/route/show_edit.php",{source:source,destination:destination})
      .done(function( data ) {
        $('#show-page').html(data);
    });
  }

  function reset(){
    $('#s_source').val("");
    $('#s_destination').val("");
    show();
  }

  $('#form-data').on('submit', function(event) {
    event.preventDefault();
    //alert("xxx"):
    if ($('#form-data').smkValidate()) {
        $.smkProgressBar();
        $.ajax({
            url: 'ajax/route/manage_edit.php',
            type: 'POST',
            data: new FormData( this ),
            processData: false,
            contentType: false,
            dataType: 'json'
        }).done(function( data ) {
          setTimeout(function(){
            $.smkProgressBar({status:'end'});
            $('#form-data').smkClear();
            show();
            $.smkAlert({text: data.message,type: data.status});
            $('#myModal').modal('toggle');
          }, 1000);
        }).fail(function() {
          $.smkProgressBar({status:'end'});
        });
    }
  });

  $(document).ready(function() {
    $("#panel1").smkPanel({
      hide: 'full,remove'
    });
    show();
  });
