function reset(){
  $('#startDate').val($('#startDateTmp').val());
  $('#endDate').val($('#endDateTmp').val());
  $('#employeeId').val("");
  $('#jobOrderNo').val("");
  $('#dp').val("");
  $('#trailerId').val("");
  $('#awaySpecNo').val("");
  $('#cust_id').val("");
  $('#type_job').val("");
  search();
}

function search(){
  var startDate     = $('#startDate').val();
  var endDate       = $('#endDate').val();
  var employeeId    = $('#employeeId').val();
  var dp            = $('#dp').val();
  var trailerId     = $('#trailerId').val();
  var awaySpecNo    = $('#awaySpecNo').val();
  var cust_id       = $('#cust_id').val();
  var type_job      = $('#type_job').val();

  $.post("ajax/report/rep02_list.php",{
    startDate:startDate,
    endDate:endDate,
    employeeId:employeeId,
    dp:dp,
    trailerId:trailerId,
    awaySpecNo:awaySpecNo,
    cust_id:cust_id,
    type_job:type_job
  })
    .done(function( data ) {
      $('#show-page').html(data);
  });
}

function exportExcel(){
  var startDate     = $('#startDate').val();
  var endDate       = $('#endDate').val();
  var employeeId    = $('#employeeId').val();
  var dp            = $('#dp').val();
  var trailerId     = $('#trailerId').val();
  var awaySpecNo    = $('#awaySpecNo').val();
  var cust_id       = $('#cust_id').val();
  var type_job      = $('#type_job').val();

  var pam = "startDate="+startDate+"&endDate="+endDate+"&employeeId="+employeeId+"&dp="+dp+"&trailerId="+trailerId+"&awaySpecNo="+awaySpecNo+"&cust_id="+cust_id+"&type_job="+type_job;
  var url = "ajax/report/rep02_list_excel.php?"+pam;
  //alert(url);
  window.open(url,'_blank');
}


function checkNumeric(num){
  var number = 0;
  if($.isNumeric(num)){
    number = parseInt(num);
  }
  return number;
}

function printDiv(divName) {

     var originalContents = document.body.innerHTML;
     //document.getElementById("tableDisplay").style.fontSize = "7px";
     document.getElementById("hd1").style.fontSize = "13px";
     document.getElementById("hd2").style.fontSize = "13px";

     var x = document.getElementsByClassName("tabf");
     for(var i=0 ; i < x.length ; i++ ){
       //alert(i);
       x[i].style.fontSize = "11px";
       x[i].style.width = "100%";
       x[i].style.border = "0px";
       x[i].classList.remove("table-bordered");
     }

     var x = document.getElementsByClassName("tdb");
     for(var i=0 ; i < x.length ; i++ ){
       //alert(i);
       //x[i].style.width = "auto";
       x[i].style.border = "1px solid black";
     }

     var printContents = document.getElementById(divName).innerHTML;
     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
}


function printOrder(){
  var startDate     = $('#startDate').val();
  var endDate       = $('#endDate').val();
  var employeeId    = $('#employeeId').val();
  var dp            = $('#dp').val();
  var trailerId     = $('#trailerId').val();
  var awaySpecNo    = $('#awaySpecNo').val();
  var cust_id       = $('#cust_id').val();
  $.post("ajax/report/rep02_list_orderExp.php",{
      startDate:startDate,
      endDate:endDate,
      employeeId:employeeId,
      dp:dp,
      trailerId:trailerId,
      awaySpecNo:awaySpecNo,
      cust_id:cust_id
  })
    .done(function( data ) {
      $('#show-page-exp').html(data);
      printDiv('printableArea');
  });
}


$("#panel2").smkPanel({
  hide: 'full,remove'
});

function toDate(dateStr) {
    var parts = dateStr.split("/");
    return parts[2]+"/"+parts[1]+"/"+parts[0];
}
