function reset(){
  $('#startDate').val($('#startDateTmp').val());
  $('#endDate').val($('#endDateTmp').val());
  $('#employeeId').val("");
  $('#jobOrderNo').val("");
  $('#dp').val("");
  $('#trailerId').val("");
  $('#awaySpecNo').val("");
  $('#cust_id').val("");
  $('#type_job').val("");
  search();
}

function search(){
  var startDate     = $('#startDate').val();
  var endDate       = $('#endDate').val();
  var employeeId    = $('#employeeId').val();
  var dp            = $('#dp').val();
  var trailerId     = $('#trailerId').val();
  var awaySpecNo    = $('#awaySpecNo').val();
  var cust_id       = $('#cust_id').val();
  var type_job      = $('#type_job').val();
  var invoice_code  = $('#invoice_code').val();


  $.post("ajax/rechk/list.php",{
    startDate:startDate,
    endDate:endDate,
    employeeId:employeeId,
    dp:dp,
    trailerId:trailerId,
    awaySpecNo:awaySpecNo,
    cust_id:cust_id,
    type_job:type_job,
    invoice_code:invoice_code
  })
    .done(function( data ) {
      $('#show-page').html(data);
  });
}

function checkAccuracy(obj,id)
{
  var value = "";
  if(obj.checked){
    value = "Y";
  }
  $.post("ajax/rechk/updateAccuracy.php",{
    id:id,value:value
  })
    .done(function( data ) {
      //
  });
}

function checkNumeric(num){
  var number = 0;
  if($.isNumeric(num)){
    number = parseInt(num);
  }
  return number;
}

function toDate(dateStr) {
    var parts = dateStr.split("/");
    return parts[2]+"/"+parts[1]+"/"+parts[0];
}
