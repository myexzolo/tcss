function showFormJobOrder(id,page){
  postURL('job_order.php?id='+id+"&page="+page);
}

function show(date,status){
  $.post("ajax/po/list.php",{startDate:date,status:status})
    .done(function( data ) {
      $('#show-page').html(data);
  });
}

function reset(){
  var startDate     = $('#startDate').val($('#startDateTmp').val());
  var endDate       = $('#endDate').val($('#endDateTmp').val());
  search();
}

function resetShipment(){
  var startDate     = $('#startDate').val($('#startDateTmp').val());
  var endDate       = $('#endDate').val($('#endDateTmp').val());
  searchShipment();
}

function searchShipment(){
  var startDate     = $('#startDate').val();
  var endDate       = $('#endDate').val();
  var shipmentCode  = $('#shipmentCode').val();
  var companyName   = $('#companyName').val();
  var shipmentDate  = $('#shipmentDate').val();
  $.post("ajax/shipment/listShipment.php",{startDate:startDate,endDate:endDate,shipmentCode:shipmentCode,companyName:companyName,shipmentDate:shipmentDate})
    .done(function(data) {
      $('#show-page').html(data);
  });
}


function checkAll(obj) {
  if(obj.checked){
    var length = $('.checkJobId').length;

    if(length > 10){
      var num = 0;
      $('.checkJobId').each(function(){
        if(num == 10){
          return false;
        }else{
          $(this).prop('checked', true);
          num++;
        }
      });
    }else{
      $('.checkJobId').prop('checked', true);
    }
  }else{
    $('.checkJobId').prop('checked', false);
  }
}

function edit(shipmentCode,companyName,shipmentDate){
  var url = "shipment.php?shipmentCode="+shipmentCode+"&companyName="+companyName+"&shipmentDate="+shipmentDate;
  postURL(url, null);
}

function search(){
  var startDate     = $('#startDate').val();
  var endDate       = $('#endDate').val();

  $.post("ajax/shipment/list.php",{startDate:startDate,endDate:endDate})
    .done(function( data ) {
      $('#show-page').html(data);
  });
}

function showForm(){
  $.post("ajax/po/showJobRoute.php")
    .done(function( data ) {
      $('#myModal').modal('toggle');
      $('#show-form').html(data);
      $('#tableDisplay').show();
  });
}


function checkNumeric(num){
  var number = 0;
  if($.isNumeric(num)){
    number = num;
  }
  return number;
}

function printShipMent(shipmentCode){
  $.post("ajax/shipment/listExp.php",{shipmentCode:shipmentCode})
    .done(function( data ) {
      $('#show-page-exp').html(data);
      printDiv('printableArea');
  });
}


function printDiv(divName) {
     var originalContents = document.body.innerHTML;
     document.getElementById("tableDisplayExp").style.fontSize = "11px";
     var printContents = document.getElementById(divName).innerHTML;
     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
}

function printInvice(){
  var receive_id = $('#receive_id').val();
  var data = $('#data').val();
  var name = $('#name').val();
  var invoice_code = $('#invoiceCode').val();
  var taxId = $('#taxId').val();
  var address = $('#address').html();
  alert(address);
  //var pam = "";
  var pam = "?receive_id="+receive_id+"&&name="+name+"&&data="+data+"&&invoice_code="+invoice_code+"&&taxId="+taxId+"&&address="+address;
  //alert(pam);
  //var myWindow = window.open("pos_process.php"+pam, "", "width=5,height=5");
  var myWindow = window.open("print_invoice.php"+pam,'', 'toolbar=no,status=no,menubar=no,scrollbars=no,resizable=no,visible=none', '');
  $('#myModal').modal('hide');
  $('#formAdd').smkClear();
}


$('#form-data').on('submit', function(event) {
  event.preventDefault();
  if ($('#form-data').smkValidate()) {
    $.ajax({
        url: 'ajax/shipment/manage.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {
      $.smkProgressBar();
      setTimeout(function(){
        $.smkProgressBar({status:'end'});
        gotoPage("shipment_list.php");
        $.smkAlert({text: data.message,type: data.status});
      }, 1000);
    }).fail(function (jqXHR, exception) {

    });
  }
});


$("#panel2").smkPanel({
  hide: 'full,remove'
});

function toDate(dateStr) {
    var parts = dateStr.split("/");
    return parts[2]+"/"+parts[1]+"/"+parts[0];
}
