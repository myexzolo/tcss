
function show(){
    var route_id = $('#route_id').val();
    $.post("ajax/route/show_rate.php",{route_id:route_id})
      .done(function( data ) {
        $('#show-page').html(data);
    });
  }


  $('#form-data').on('submit', function(event) {
    event.preventDefault();
    //alert("xxx"):
    if ($('#form-data').smkValidate()) {
        $.smkProgressBar();
        $.ajax({
            url: 'ajax/route/manage_rate.php',
            type: 'POST',
            data: new FormData( this ),
            processData: false,
            contentType: false,
            dataType: 'json'
        }).done(function( data ) {
          setTimeout(function(){
            $.smkProgressBar({status:'end'});
            //$('#form-data').smkClear();
            //console.log
            show();
            $.smkAlert({text: data.message,type: data.status});
            $('#myModal').modal('toggle');
          }, 1000);
        }).fail(function() {
          $.smkProgressBar({status:'end'});
        });
    }
  });

  $(document).ready(function() {
    $("#panel1").smkPanel({
      hide: 'full,remove'
    });
    show();
  });
