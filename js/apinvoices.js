function reset(){
  $('#departmentId').val("");
  search();
}

function expApInvoice(apInvoiceNo){
  var typeCompany = $('#typeCompany').val();
  var url = "";
  if(typeCompany == 1){
    url = "ajax/apinvoices/listExp.php";
  }else{
    url = "ajax/apinvoices/listExpPost.php";
  }
  console.log(url);
  $.post(url,{apInvoiceNo:apInvoiceNo})
    .done(function( data ) {
      $('#show-page-exp').html(data);
      printDiv('printableArea');
  });
}

function editApInvoice(apInvoiceNo){
  var typeCompany = $('#typeCompany').val();
  var url = "";
  if(typeCompany == 1){
    url = "apinvoices.php?apInvoiceNo="+apInvoiceNo;
  }else{
    url = "apinvoices.php?apInvoiceNo="+apInvoiceNo;
  }
  postURL(url, null);
}


function search(){
  var departmentId  = $('#departmentId').val();
  var startDate     = $('#startDate').val();
  var endDate       = $('#endDate').val();

  $.post("ajax/apinvoices/list.php",{department_id:departmentId,startDate:startDate,endDate:endDate})
  .done(function(data) {
      $('#show-page').html(data);
  });
}

function reset2()
{
  $('#departmentId').val("");
  $('#trailer_id').val("");
  $('#contr').val("");
  $('.select2').select2();
  searchApInvoice();
}

function chkUndefined(val) {
  if (typeof val === "undefined")
  {
    val = "";
  }
  return val;
}

function searchApInvoice(){
  var departmentId      = chkUndefined($('#departmentId').val());
  var apInvoiceNo       = chkUndefined($('#apInvoiceNo').val());
  var apinvoiceDate     = chkUndefined($('#apinvoiceDate').val());
  var remark            = chkUndefined($('#remark').val());
  var trailer_id        = chkUndefined($('#trailer_id').val());
  var contr             = chkUndefined($('#contr').val());
  var startDate         = chkUndefined($('#startDate').val());
  var endDate           = chkUndefined($('#endDate').val());
  //alert(departmentId);
  $.post("ajax/apinvoices/listApInvoice.php",{
    department_id:departmentId,
    apInvoiceNo:apInvoiceNo,
    apinvoiceDate:apinvoiceDate,
    remark:remark,
    trailer_id:trailer_id,
    startDate:startDate,
    endDate:endDate,
    contr:contr
  })
    .done(function(data) {
      $('#show-page').html(data);
      if(apInvoiceNo != ""){
        cal();
      }
  });
}


function clearDataInv(){
  $('#apInvoiceNo').val("");
  $('#apinvoiceDate').val("");
}

function checkNumeric(num){
  var number = 0;
  if($.isNumeric(num)){
    number = num;
  }
  return number;
}

function checkAll(obj) {
  if(obj.checked){
    var length = $('.checkJobId').length;

    if(length > 26){
      var num = 0;
      $('.checkJobId').each(function(){
        if(num == 26){
          return false;
        }else{
          $(this).prop('checked', true);
          num++;
        }
      });
    }else{
      $('.checkJobId').prop('checked', true);
    }
  }else{
    $('.checkJobId').prop('checked', false);
    $('#delivery_cost').val(0);
    $('#total_cost').val(0);
  }
  cal();
}
function chkList(obj){
  var num = 0;
  $( ".checkJobId" ).each(function( index ) {
      if(this.checked){
        num++;
      }
      if(num > 9){
        obj.checked = false;
      }
  });
}

function cal(){
  var transfer = 0;
  var num = 0;
  var discount = 0;
  $( ".checkJobId" ).each(function( index ) {
      //console.log( index + ": " + $( this ).text());
      if(this.checked){
        var id = "#net"+ $( this ).val();
        var cost = $(id).html();
        transfer += parseFloat(cost.replace(/,/g, ""));

      }
  });


  var ot          = parseFloat(checkNumeric($('#ot').val().replace(/,/g, "")));
  var provision   = parseFloat(checkNumeric($('#provision').val().replace(/,/g, "")));


  var total_cost    = parseFloat(checkNumeric($('#total_cost').val().replace(/,/g, "")));
  var installment   = parseFloat(checkNumeric($('#installment').val().replace(/,/g, "")));
  var oil_price     = parseFloat(checkNumeric($('#oil_price').val().replace(/,/g, "")));
  var gps           = parseFloat(checkNumeric($('#gps').val().replace(/,/g, "")));
  var sso           = parseFloat(checkNumeric($('#sso').val().replace(/,/g, "")));
  var warranty      = parseFloat(checkNumeric($('#warranty').val().replace(/,/g, "")));
  var insurance     = parseFloat(checkNumeric($('#insurance').val().replace(/,/g, "")));
  var register_head = parseFloat(checkNumeric($('#register_head').val().replace(/,/g, "")));
  var register_footter = parseFloat(checkNumeric($('#register_footter').val().replace(/,/g, "")));
  var act           = parseFloat(checkNumeric($('#act').val().replace(/,/g, "")));
  var tire          = parseFloat(checkNumeric($('#tire').val().replace(/,/g, "")));
  var other         = parseFloat(checkNumeric($('#other').val().replace(/,/g, "")));

  var amounts = (installment + oil_price + gps + sso + warranty + insurance + register_head + register_footter + act + tire + other);
  var n       = transfer + ot;
  discount    = (n * 0.01);
  var amount  =  n - (discount + provision + amounts);
  //alert(transfer+","+ provision+","+ ot+","+discount+","+amounts);
  $('#discount').val(discount.toFixed(2));
  $('#delivery_cost').val(transfer.toFixed(2));
  $('#total_cost').val(amount.toFixed(2));
}

function exportExcel(apinvoiceCode){
  var url = "export_excel_apinvoice.php?apinvoiceCode="+apinvoiceCode;
  window.open(url,'_blank');
}

function printDiv(divName) {

     var originalContents = document.body.innerHTML;
     document.getElementById("tableDisplayExp").style.fontSize = "11px";
     var printContents = document.getElementById(divName).innerHTML;
     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
}


$('#form-data').on('submit', function(event) {
  event.preventDefault();
  if ($('#form-data').smkValidate()) {
    cal();
    $.ajax({
        url: 'ajax/apinvoices/manage.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {
      $.smkProgressBar();
      setTimeout(function(){
        $.smkProgressBar({status:'end'});
        gotoPage('apinvoices_list.php');
        $.smkAlert({text: data.message,type: data.status});
      }, 1000);
    }).fail(function (jqXHR, exception) {
      //alert("fail");
    });
  }
});


$("#panel2").smkPanel({
  hide: 'full,remove'
});

function toDate(dateStr) {
    var parts = dateStr.split("/");
    return parts[2]+"/"+parts[1]+"/"+parts[0];
}
