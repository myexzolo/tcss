function reset(){
  var startDate     = $('#startDate').val($('#startDateTmp').val());
  var endDate       = $('#endDate').val($('#endDateTmp').val());
  var status        = $('#status').val("1");
  var employeeId    = $('#employeeId').val("");
  var affiliationId = $('#affiliationId').val("");
  search();
}


function search(){
  var startDate     = $('#startDate').val();
  var endDate       = $('#endDate').val();
  var status        = $('#status').val();
  var employeeId    = $('#employeeId').val();
  var affiliationId = $('#affiliationId').val();
  var trailer_id    = $('#trailer_id').val();
  var cust_id       = $('#cust_id').val();

  $.post("ajax/approval/list.php",{startDate:startDate,endDate:endDate,status:status,employeeId:employeeId,affiliationId:affiliationId,trailer_id:trailer_id,cust_id:cust_id})
    .done(function( data ) {
      $('#show-page').html(data);
  });
}

function checkNumeric(num){
  var number = 0;
  if($.isNumeric(num)){
    number = num;
  }
  return number;
}

function checkAll(obj) {
  if(obj.checked){
    $('.checkApprove').prop('checked', true);
  }else{
    $('.checkApprove').prop('checked', false);
  }
}



function printDiv(divName) {

     var originalContents = document.body.innerHTML;
     document.getElementById("tableDisplay").style.fontSize = "11px";
     var printContents = document.getElementById(divName).innerHTML;
     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
}

function printInvice(){
  var receive_id = $('#receive_id').val();
  var data = $('#data').val();
  var name = $('#name').val();
  var invoice_code = $('#invoiceCode').val();
  var taxId = $('#taxId').val();
  var address = $('#address').html();
  alert(address);
  //var pam = "";
  var pam = "?receive_id="+receive_id+"&&name="+name+"&&data="+data+"&&invoice_code="+invoice_code+"&&taxId="+taxId+"&&address="+address;
  //alert(pam);
  //var myWindow = window.open("pos_process.php"+pam, "", "width=5,height=5");
  var myWindow = window.open("print_invoice.php"+pam,'', 'toolbar=no,status=no,menubar=no,scrollbars=no,resizable=no,visible=none', '');
  $('#myModal').modal('hide');
  $('#formAdd').smkClear();
}


$('#form-data').on('submit', function(event) {
  event.preventDefault();
  if ($('#form-data').smkValidate()) {
    $.ajax({
        url: 'ajax/approval/manage.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {
      $.smkAlert({text: data.message,type: data.status});
      $('#checkApp').prop('checked', false);
      search();
    }).fail(function (jqXHR, exception) {
      //alert("fail");
    });
  }
});


$("#panel2").smkPanel({
  hide: 'full,remove'
});

function toDate(dateStr) {
    var parts = dateStr.split("/");
    return parts[2]+"/"+parts[1]+"/"+parts[0];
}
