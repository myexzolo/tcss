<?php
include('../../conf/connect.php');
include('../../inc/utils.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);


$docReturnCode    = isset($_POST['docReturnCode'])?$_POST['docReturnCode']:"";

$con = "";

if($docReturnCode != "")
{
  $con .= " and doc_return_code ='". $docReturnCode ."'";
}

$sql = "SELECT * FROM tb_doc_return d, tb_customer_master c
where d.cust_id = c.cust_id $con order by doc_return_code DESC";
//echo $sql;
$query  = mysqli_query($conn,$sql);
$num = mysqli_num_rows($query);

$row = mysqli_fetch_assoc($query);
$doc_return_date  = formatDate($row['doc_return_date'],'d/m/Y');
$cust_id          = $row['cust_id'];//รหัสลูกค้า
$cust_name        = $row['cust_name'];

?>
<br><br>
<table style="width:100%;margin-bottom:10px;">
  <tr>
    <td width="20%"></td>
    <td width="60%" style="font-size:16px;" align="center"><p><b>ใบสั่งจ่ายสินค้า</b></p></td>
    <td width="20%" align="right" style="font-size:14px;">วันที่ <?= $doc_return_date ?></td>
  </tr>
  <tr>
    <td colspan="3" style="padding:5px;"></td>
  </tr>
  <tr>
    <td colspan="3" style="font-size:14px;" align="left"><p><b>หจก.โชควิรัตน์ทรานสปอร์ต</b></p></td>
  </tr>
  <tr>
    <td colspan="3" style="font-size:14px;"><b><?= $cust_name ?></b></td>
  </tr>
</table>
<table style="width:100%;font-size:11px;" id="tableDisplayExp">
  <thead>
    <tr>
			<th colspan="8" style="height:10px;">&nbsp;</th>
		</tr>
    <tr>
			<th style="border: 1px solid;padding:5px;width:30px;vertical-align: middle;">ลำดับ</th>
      <th style="border: 1px solid;padding:5px;width:80px;vertical-align:middle;">วันที่สั่งจ้าง</th>
      <th style="border: 1px solid;padding:5px;vertical-align:middle;">เลขที่ Shipment</th>
      <th style="border: 1px solid;padding:5px;vertical-align:middle;">เลขที่ใบจ่าย (DP)</th>
			<th style="border: 1px solid;padding:5px;vertical-align: middle;">ต้นทาง</th>
			<th style="border: 1px solid;padding:5px;vertical-align:middle;">ปลายทาง</th>
			<th style="border: 1px solid;padding:5px;width:55px;vertical-align:middle;">จำนวนตัน/เที่ยว</th>
			<th style="border: 1px solid;padding:5px;vertical-align:middle;display:none;">เลขที่ใบส่งสินค้า</th>
      <th style="border: 1px solid;padding:5px;width:60px;vertical-align:middle;">ทะเบียนรถ</th>
		</tr>
  </thead>
  <tbody>
<?php
  //$sql = "SELECT * FROM tb_po_customer pc, tb_customer_master cm, tb_employee_master em where pc.Cust_ID = cm.Cust_ID and pc.Employee_No = em.Employee_No $con ";
  $sql = "SELECT jo.*,em.employee_name,t.license_plate
  FROM tb_job_order jo, tb_employee_master em, tb_trailer t
  where jo.employee_id = em.employee_id $con and  jo.trailer_id = t.trailer_id  $con
  order by jo.shipment + 0,jo.job_order_date";
  //echo $sql;
  $query  = mysqli_query($conn,$sql);
  $num = mysqli_num_rows($query);

  $totalAmont = 0;
  for ($i=1; $i <= $num ; $i++) {
    $row = mysqli_fetch_assoc($query);
    $Employee_Name      = $row['employee_name'];
    $license_plate      = $row['license_plate'];
    $id                 = $row['id'];
    $job_order_no       = $row['job_order_no'];
    $delivery_type_id   = $row['delivery_type_id'];//รูปแบบการขนส่ง
    $job_order_date     = formatDate($row['job_order_date'],'d/m/y');//วันที่ออกใบสั่งงาน
    $job_delivery_date  = formatDate($row['job_delivery_date'],'d/m/Y');;//วันที่กำหนดส่งสินค้า
    $cust_id            = $row['cust_id'];//รหัสลูกค้า
    $employee_id        = $row['employee_id'];//รหัสพนักงาน
    $trailer_id         = $row['trailer_id'];//หัว หมายเลขทะเบียนรถ
    $away_regi_no       = $row['away_regi_no'];//หาง
    $away_spec_no       = $row['away_spec_no'];//สเปคหาง
    $affiliation_id     = $row['affiliation_id'];//รหัส รถ(สังกัด)
    $route_id           = $row['route_id'];//รหัสเส้นทาง
    $source             = $row['source'];//ต้นทาง
    $destination        = $row['destination'];//ปลายทาง
    $distance           = $row['distance'];//ระยะทาง
    $cust_dp            = $row['cust_dp'];//DP
    $delivery_doc       = $row['delivery_doc'];//ใบส่งของ
    $department         = $row['department'];//หน่วยงาน
    $product_name       = $row['product_name'];//สินค้า
    $number_of_trips    = $row['number_of_trips'];//จำนวนเที่ยว
    $weights            = $row['weights'];//น้ำหนัก(ตัน)
    $shipping_amount    = $row['shipping_amount'];//ราคาค่าขนส่ง
    $date_product_in    = $row['date_product_in'];//วันทีรับสินค้า
    $time_product_in    = $row['time_product_in'];//เวลารับสินค้า
    $date_product_out   = $row['date_product_out'];//วันที่ออกสินค้า
    $time_product_out   = $row['time_product_out'];//เวลาออกสินค้า
    $date_delivery_in   = formatDate($row['date_delivery_in'],'d/m/Y');//วันทีส่งสินค้า
    $time_delivery_in   = $row['time_delivery_in'];//เวลาเข้าส่งสินค้า
    $date_delivery_out  = $row['date_delivery_out'];//วันที่ออกส่งสินค้า
    $time_delivery_out  = $row['time_delivery_out'];//เวลาออกส่งสินค้า
    $fuel_cost          = $row['fuel_cost'];//ค่าน้ำมัน
    $fuel_litre         = $row['fuel_litre'];//จำนวนลิตร
    $blank_charge       = $row['blank_charge'];//ค่าตีเปล่า
    $allowance          = $row['allowance'];//เบี้ยเลี้ยง
    $kog_expense        = $row['kog_expense'];//หางคอก
    $allowance_oth      = $row['allowance_oth'];//เบี้ยงเลี้ยงอื่นๆ
    $remark             = $row['remark'];
    $shipment           = $row['shipment'];
    $price_type_id      = $row['price_type_id'];//รหัสการคำนวณ
    $one_trip_ton       = $row['one_trip_ton'];//ราคาต่อตัน
    $ext_one_trip_ton   = $row['ext_one_trip_ton'];//ราคารถร่วมต่อตัน
    $price_per_trip     = $row['price_per_trip'];//ราคาต่อเที่ยว
    $ext_price_per_trip = $row['ext_price_per_trip'];//ราคารถร่วมต่อเที่ยว
    $total_amount_receive = $row['total_amount_receive'];//ราคาค่าขนส่ง
    $total_amount_ap_pay  = $row['total_amount_ap_pay'];//ราคาจ่ายรถร่วม
    $total_amount_allowance   = $row['total_amount_allowance'];//รวมค่าใช้จ่าย
    $job_ended_clearance      = $row['job_ended_clearance'];//ค่าเคลียร์ค่าปรับ
    $job_ended_recap          = $row['job_ended_recap'];//ค่าปะยาง
    $job_ended_expressway     = $row['job_ended_expressway'];//ค่าทางด่วน
    $job_ended_passage_fee    = $row['job_ended_passage_fee'];//ค่าธรรมเนียมผ่านท่าเรือ
    $job_ended_repaires       = $row['job_ended_repaires'];//ค่าซ่อม
    $job_ended_acc_expense    = $row['job_ended_acc_expense'];//ค่าทำบัญชี
    $job_ended_other_expense  = $row['job_ended_other_expense'];//คชจ.อื่นๆ
    $fuel_driver_bill         = $row['fuel_driver_bill'];//บิลน้ำมันจากคนขับ
    $job_status_id            = $row['job_status_id'];

    $period_code              = $row['period_code'];


    $price = 0;
    $amount = 0;
    if($price_type_id == 1){
      $amount =  $weights;
    }else{
      $amount = 1;
    }

?>
  <tr>
    <td style="border: 1px solid;padding:5px;vertical-align:top;" class="text-center"><?= $i ?></td>
    <td style="border: 1px solid;padding:5px;vertical-align:top;" class="text-left" ><?= $job_order_date ?></td>
    <td style="border: 1px solid;padding:5px;vertical-align:top;" class="text-center" ><?= $shipment ?></td>
    <td style="border: 1px solid;padding:5px;vertical-align:top;" class="text-left" ><?= $cust_dp ?></td>
    <td style="border: 1px solid;padding:5px;vertical-align:top;" class="text-left" ><?= $source ?></td>
    <td style="border: 1px solid;padding:5px;vertical-align:top;" class="text-left" ><?= $destination ?></td>
    <td style='border: 1px solid;padding:5px;vertical-align:top;' class="text-right" ><?= number_format($amount,3);?></td>
    <td style="border: 1px solid;padding:5px;vertical-align:top;display:none;" class="text-left" ><?= $delivery_doc  ?></td>
    <td style="border: 1px solid;padding:5px;vertical-align:top;" class="text-left" ><?= $license_plate ?></td>
  </tr>
<?php }
?>
<tr>
  <td colspan="8" style="padding:5px;"><p>จำนวนรายการ <?= ($i -1) ?> รายการ</p></td>
</tr>
<tr>
  <td colspan="8" style="padding:5px;">&nbsp;</td>
</tr>
<tr>
  <td colspan="5" style="border: 0px solid;padding:5px;font-size:12px;"></td>
  <td colspan="3" style="padding:5px;font-size:12px;" class="text-center">
    <p>(......................................................................)<br>
    <p>ลงชื่อ</p>
  </td>
</tr>
</tbody>
</table>
