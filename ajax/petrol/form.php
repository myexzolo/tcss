<?php

include('../../conf/connect.php');
session_start();

header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
?>

<?php
$petrol_code  = "";
$petrol_name  = "";


$petrol_id = isset($_POST['value'])?$_POST['value']:"";

if(!empty($petrol_id))
{
    $sql = "SELECT * FROM tb_petrol_master WHERE petrol_id = '$petrol_id'";
    $query = mysqli_query($conn,$sql);
    $row = mysqli_fetch_assoc($query);

    $petrol_code    = $row['petrol_code'];
    $petrol_name    = $row['petrol_name'];
    $petrol_group   = $row['petrol_group'];
}
?>
<div class="row">
  <div class="col-md-3">
    <div class="form-group">
      <label>รหัส</label>
      <input value="<?= $petrol_code ?>" name="petrol_code" type="text" class="form-control" placeholder="รหัส" required>
    </div>
  </div>
  <div class="col-md-3">
    <div class="form-group">
      <label>ปั้มน้ำมัน</label>
      <select name="petrol_group" class="form-control select2" style="width: 100%;" required>
        <option value="SH" <?= ($petrol_group == 'SH' ? 'selected="selected"':'') ?> >shell</option>
        <option value="BG" <?= ($petrol_group == 'BG' ? 'selected="selected"':'') ?> >บางจาก</option>
        <option value="PT" <?= ($petrol_group == 'PT' ? 'selected="selected"':'') ?> >PT</option>
        <option value="BB" <?= ($petrol_group == 'BB' ? 'selected="selected"':'') ?> >อู่บางบาล</option>
        <option value="PTT" <?= ($petrol_group == 'PTT' ? 'selected="selected"':'') ?> >ปตท.</option>
        <option value="ES" <?= ($petrol_group == 'ES' ? 'selected="selected"':'') ?> >ESSO</option>
      </select>
    </div>
  </div>
  <div class="col-md-6">
    <div class="form-group">
      <label>ชื่อปั้มน้ำมัน</label>
      <input value="<?= $petrol_name ?>" name="petrol_name" type="text" class="form-control" placeholder="ชื่อปั้มน้ำมัน" required>
    </div>
  </div>
</div>

<input  type="hidden" value="<?= $petrol_id ?>" name="petrol_id" type="text" >
