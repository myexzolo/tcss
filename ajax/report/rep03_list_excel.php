<?php
include('../../conf/connect.php');
include('../../inc/utils.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$strExcelFileName="export_petrol.xls";

header("Content-Type: application/x-msexcel; name=\"$strExcelFileName\"");
header("Content-Disposition: inline; filename=\"$strExcelFileName\"");
header("Pragma:no-cache");

$startDate      = isset($_GET['startDate'])?$_GET['startDate']:"";
$endDate        = isset($_GET['endDate'])?$_GET['endDate']:"";
$petrol_group   = isset($_GET['petrol_group'])?$_GET['petrol_group']:"";
$trailer_id     = isset($_GET['trailer_id'])?$_GET['trailer_id']:"";
$employeeId     = isset($_GET['employeeId'])?$_GET['employeeId']:"";
$route_id       = isset($_GET['route_id'])?$_GET['route_id']:"";
$cust_id        = isset($_GET['cust_id'])?$_GET['cust_id']:"";
$petrol_code    = isset($_GET['petrol_code'])?$_GET['petrol_code']:"";

$con = "";
$cusName = "";
$nump = 0;
if($startDate != "")
{
  $con = " and jo.job_order_date between '". $startDate ."' and '". $endDate ."' ";
}

if($trailer_id != "")
{
  $con .= " and jo.trailer_id = '". $trailer_id ."'";
}

if($employeeId != "")
{
  $con .= " and jo.employee_id = '". $employeeId ."'";
}

if($route_id != "")
{
  $con .= " and jo.route_id = '". $route_id ."'";
}

if($cust_id != "")
{
  $con .= " and jo.cust_id = '". $cust_id ."'";
}
$conp = "";
if($petrol_code != ""){
  $conp  = " and petrol_code = '".$petrol_code."' ";
}

$petrolCodeList = "";

$petrolCode = array();
$petrolArr = array();

if($petrol_group != "")
{
  $sql = "SELECT * FROM tb_petrol_master  where  petrol_group ='". $petrol_group ."' $conp";
  $queryp   = mysqli_query($conn,$sql);
  $nump     = mysqli_num_rows($queryp);

  for ($i=0; $i < $nump ; $i++) {
    $row = mysqli_fetch_assoc($queryp);

    $petrolCode[$i] = $row;
    $petrolArr[$i]['tl']  = 0;
    $petrolArr[$i]['p']   = 0;
    $petrolArr[$i]['tp']  = 0;
    if($petrolCodeList == ""){
      $petrolCodeList = "'".$row['petrol_code']."'";
    }else{
      $petrolCodeList .= ",'".$row['petrol_code']."'";
    }
  }
}else{
  $sql = "SELECT p.petrol_code , p.petrol_station as petrol_name
  FROM tb_job_order jo ,tb_petrol p
  where jo.job_order_no = p.job_order_no  $con  $conp
  GROUP BY petrol_code";
  //echo $sql;
  $queryp   = mysqli_query($conn,$sql);
  $nump     = mysqli_num_rows($queryp);

  for ($i=0; $i < $nump ; $i++) {
    $row = mysqli_fetch_assoc($queryp);
    $petrol_code = $row['petrol_code'];
    if($petrol_code == ""){

    }
    $petrolCode[$i] = $row;
    $petrolArr[$i]['tl']  = 0;
    $petrolArr[$i]['p']   = 0;
    $petrolArr[$i]['tp']  = 0;
    if($petrolCodeList == ""){
      $petrolCodeList = "'".$row['petrol_code']."'";
    }else{
      $petrolCodeList .= ",'".$row['petrol_code']."'";
    }
  }
}

$petroName = "";
if($petrol_group == "SH")
{
  $petroName = "shell";
}
else if($petrol_group == "BG")
{
  $petroName = "บางจาก";
}
else if($petrol_group == "PT")
{
  $petroName = "PT";
}
else if($petrol_group == "BB")
{
  $petroName = "อู่บางบาล";
}
else if($petrol_group == "PTT")
{
  $petroName = "ปตท.";
}
else if($petrol_group == "ES")
{
  $petroName = "ESSO";
}

?>
<html xmlns:o="urn:schemas-microsoft-com:office:office"xmlns:x="urn:schemas-microsoft-com:office:excel"xmlns="http://www.w3.org/TR/REC-html40">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
<div id="SiXhEaD_Excel" align=center x:publishsource="Excel">
  <table id="tableDisplay">
    <thead>
    <tr class="text-center">
      <td style="font-size:16px;" align="center" colspan="<?= ($nump * 3) + 7 ?>">
          <b>รายงานสรุปการใช้น้ำมัน <?= $petroName ?></b>
      </td>
    </tr>
    <tr>
      <td style="font-size:16px;" align="center" colspan="<?= ($nump * 3) + 7 ?>">
        <b>
          ประจำวันที่ <?= formatDateTh($startDate) ?> ถึงวันที่ <?= formatDateTh($endDate) ?>
        </b>
      </td>
    </tr>
</table>
<table id="tableDisplay" border="1">
  <thead>
    <tr class="text-center">
      <th rowspan="2" class="text-center" style="vertical-align:middle;" >วันที่</th>
      <th rowspan="2" class="text-center" style="vertical-align:middle;">ทะเบียน</th>
      <th rowspan="2" class="text-center" style="vertical-align:middle;">ประเภทรถ</td>
      <th rowspan="2" class="text-center" style="vertical-align:middle;">ชื่อพนักงานขับรถ</td>
      <th rowspan="2" class="text-center" style="vertical-align:middle;">หน่วยงาน</th>
      <th rowspan="2" class="text-center" style="vertical-align:middle;">ต้นทาง</th>
      <th rowspan="2" class="text-center" style="vertical-align:middle;">ปลายทาง</th>
      <?php
        for ($i=0; $i < $nump ; $i++) {
            echo "<th colspan='3' class='text-center tdb' >".$petrolCode[$i]['petrol_name']."</th>";
        }
      ?>
    </tr>

      <?php
      if($nump > 0){
        echo "<tr class='text-center'>";
        for ($i=0; $i < $nump ; $i++) {
            echo "<th class='text-center' >จำนวน<br>ลิตรที่เติม</th>";
            echo "<th class='text-center' >ราคา/<br>ลิตร</th>";
            echo "<th class='text-center' >จำนวนเงิน<br>ตามปั้ม</th>";
        }
        echo "</tr>";
      }
      ?>
  </thead>
  <tbody>
    <?php
    if($nump > 0){
        $sql = "SELECT jo.job_order_no,jo.job_order_date ,jo.source,jo.department,jo.destination,
        em.employee_name, p.*, t.license_plate, t.nature
        FROM tb_petrol p, tb_job_order jo, tb_employee_master em, tb_trailer t
        where p.petrol_code in (".$petrolCodeList.") and p.job_order_no = jo.job_order_no
        and jo.employee_id = em.employee_id $con and jo.trailer_id = t.trailer_id
        order by jo.job_order_date , p.job_order_no";
        //echo $sql;
        $query   = mysqli_query($conn,$sql);
        $num     = mysqli_num_rows($query);
        //echo   $num;
        for ($i=1; $i <= $num ; $i++) {
          $row = mysqli_fetch_assoc($query);
          $job_order_no         = $row['job_order_no'];
          $job_order_date       = date('d/m/Y', strtotime($row['job_order_date']));
          $source               = $row['source'];
          $destination          = $row['destination'];
          $Employee_Name        = $row['employee_name'];
          $department           = $row['department'];
          $license_plate        = $row['license_plate'];//หมายเลขทะเบียนรถ
          $nature = $row['nature'];//ประเภทรถ

          $petrol_code   = $row['petrol_code'];//ประเภทรถ
          $fuel_cost     = $row['fuel_cost'];//ค่าน้ำมัน
          $fuel_litre    = $row['fuel_litre'];//จำนวนลิตร
    ?>
        <tr class="text-center tdb">
          <td align="center"  class="tdb"><?= $job_order_date; ?></td>
          <td align="center"  class="tdb"><?= $license_plate; ?></td>
          <td align="center"  class="tdb"><?= $nature; ?></td>
          <td align="left"  class="tdb"><?= $Employee_Name; ?></td>
          <td align="center"  class="tdb"><?= $department; ?></td>
          <td align="left"  class="tdb"><?= $source; ?></td>
          <td align="left"  class="tdb"><?= $destination; ?></td>
    <?php
    for ($x=0; $x < $nump ; $x++) {
        $tl = '';
        $p = '';
        $tp = '';
        if($petrol_code == $petrolCode[$x]['petrol_code'] && $petrol_code != ""){
          $tl = $fuel_litre;
          $p  = number_format($fuel_cost,2);
          $tp = number_format(($fuel_litre * $fuel_cost),2);

          $petrolArr[$x]['tl']  += $tl;
          $petrolArr[$x]['p']   += $fuel_cost;
          $petrolArr[$x]['tp']  += ($fuel_litre * $fuel_cost);
        }
        echo "<td class='text-right tdb' >$tl</td>";
        echo "<td class='text-right tdb' >$p</td>";
        echo "<td class='text-right tdb' >$tp</td>";
    }
     ?>
        </tr>
    <?php
        }
    ?>
    <tr class="text-center tdb">
      <td colspan="7" align="center"  class="tdb"><b>รวม</b></td>
   <?php
       $ttl = 0;
       $ttp = 0;

       for ($x=0; $x < $nump  ; $x++) {
           $tl = number_format($petrolArr[$x]['tl'],2);
           $p = number_format($petrolArr[$x]['p'],2);
           $tp = number_format($petrolArr[$x]['tp'],2);

           $ttl += $petrolArr[$x]['tl'];
           $ttp += $petrolArr[$x]['tp'];

           echo "<td class='text-right tdb' >$tl</td>";
           echo "<td class='text-right tdb' >$p</td>";
           echo "<td class='text-right tdb' >$tp</td>";
       }

       $numTD = ($nump * 3) + 4;
   ?>

    </tr>
    <tr class="text-center tdb">
      <td colspan="<?= $numTD ?>" align="right"  class="tdb"><b>รวมจำนวนลิตร</b></td>
      <td colspan="3" align="right"  class="tdb"><b><?= number_format($ttl,2) ?></b></td>
    </tr>
    <tr class="text-center tdb">
      <td colspan="<?= $numTD ?>" align="right"  class="tdb"><b>รวมจำนวนเงิน</b></td>
      <td colspan="3" align="right"  class="tdb"><b><?= number_format($ttp,2) ?></b></td>
    </tr>
    <?php
      }
    ?>
  </tbody>
</table>
<script>
window.onbeforeunload = function(){return false;};
setTimeout(function(){window.close();}, 10000);
</script>
</div>
</body>
</html>
