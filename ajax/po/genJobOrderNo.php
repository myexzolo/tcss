<?php

include('../../conf/connect.php');
include('../../inc/utils.php');

$dateOrder = $_POST['dateOrder'];
//$dateOrder = "2018-10-16";

$d        = date('Y-m-d',strtotime($dateOrder));
$strDay   = date('d',strtotime($d));
$strMonth = date('m',strtotime($d));
$strYear  = date("Y",strtotime($d));
if($strYear < 2500){
  $strYear += 543;
}
$strYear  = substr($strYear,2,2);

$Code = "TT".$strYear.$strMonth.$strDay;

$job_status_id  = "1";

//echo $Code;

$sql = "SELECT max(job_order_no) as job_order_no FROM tb_job_order where job_order_no LIKE '$Code%'";
$query  = mysqli_query($conn,$sql);
$row    = mysqli_fetch_assoc($query);
$jobOrderNo  = $row['job_order_no'];

if(!empty($jobOrderNo)){
  $lastNum = substr($jobOrderNo,strlen($Code));
  $lastNum = $lastNum + 1;
  $job_order_no = $Code.sprintf("%03d", $lastNum);
}else{
  $job_order_no = $Code.sprintf("%03d", 1);
}

if(mysqli_query($conn,$sql)){
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'success','message' => 'Success', 'jobOrderNo'=> $job_order_no)));
}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'danger','message' => 'Fail','jobOrderNo'=> '')));
}
?>
