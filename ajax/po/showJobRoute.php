<?php

include('../../conf/connect.php');
include("../../inc/utils.php");
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
?>
<table class="table table-bordered table-striped" id="tableDisplay" style="display:none;">
  <thead>
    <tr>
      <th></th>
      <th>รหัสเส้นทาง</th>
      <th>ต้นทาง</th>
      <th>ปลายทาง</th>
      <th>ระยะทาง</th>
      <th>เบี้ยเลี้ยง</th>
      <th>ค่าคอก</th>
      <th>คชจ.บัญชี</th>
    </tr>
  </thead>
  <tbody>
<?php
  $con = "";
  $searchBy   = isset($_POST['searchBy'])?$_POST['searchBy']:"";
  $searchTxt  = isset($_POST['searchTxt'])?$_POST['searchTxt']:"";
  if($searchTxt != ""){
    $con = " where ".$searchBy. " like '%".$searchTxt."%'";
  }


  $sql = "SELECT * FROM tb_route_price $con ORDER BY source,destination";

  $query = mysqli_query($conn,$sql);
  $num = mysqli_num_rows($query);
  for ($i=1; $i <= $num ; $i++) {
    $row = mysqli_fetch_assoc($query);
?>
    <tr class="text-center">
      <td><button type="button" class="btn btn-success btn-sm btn-flat"
        onclick="setDataRoute('<?= $row['route_id'] ?>','<?= $row['source'] ?>',
        '<?= $row['destination'] ?>','<?= $row['distance'] ?>',
        '<?= $row['allowance'] ?>','<?= $row['kog_expense'] ?>',
        '<?= $row['allowance_oth'] ?>','<?= $row['acc_expense'] ?>',
        '<?= $row['one_trip_ton'] ?>','<?= $row['price_per_trip'] ?>',
        '<?= $row['ext_one_trip_ton'] ?>','<?= $row['ext_price_per_trip'] ?>')">เลือก</button></td>
      <td align="left"><?= $row['route_id']; ?></td>
      <td align="left"><?= $row['source']; ?></td>
      <td align="left"><?= $row['destination']; ?></td>
      <td align="right"><?= $row['distance']; ?></td>
      <td align="right"><?= $row['allowance']; ?></td>
      <td align="right"><?= $row['kog_expense']; ?></td>
      <td align="right"><?= $row['acc_expense']; ?></td>
    </tr>
<?php } ?>
</tbody>
</table>
<script>
  $(function () {
    $('#tableDisplay').DataTable({
     'paging'      : true,
     'lengthChange': false,
     "lengthMenu": [[10,20, 25, 50, -1], [10,20, 25, 50, "All"]],
     'searching'   : false,
     'ordering'    : false,
     'info'        : true
   })
  })
</script>
