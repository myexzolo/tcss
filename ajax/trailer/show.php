<?php

include('../../conf/connect.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
?>
<table class="table table-bordered table-striped table-hover" id="tableDisplay">
  <thead>
    <tr class="text-center">
      <th style="width:40px" class="text-center">No.</th>
      <th class="text-center">รหัส </th>
      <th class="text-center">ทะเบียนรถ</th>
      <th class="text-center">จังหวัด</th>
      <th class="text-center">Model</th>
      <th class="text-center">พนักงานขับรถ</th>
      <th class="text-center">สังกัด</th>
      <th class="text-center">เจ้าของรถ</th>
      <th style="width:100px;"></th>
    </tr>
  </thead>
  <tbody>
<?php
  $sql = "SELECT t.*, p.province_name_th, a.affiliation_name, e.employee_name, d.department_name
  FROM  tb_trailer t , tb_province_master p , tb_trailer_affiliation a,  tb_employee_master e,  tb_department_master d
  where t.province_id = p.province_id  and t.affiliation_id  = a.affiliation_id  and t.employee_id = e.employee_id and t.department_id = d.department_id
  order by t.license_plate";

  $query = mysqli_query($conn,$sql);
  $num = mysqli_num_rows($query);
  for ($i=1; $i <= $num ; $i++) {
    $row = mysqli_fetch_assoc($query);
?>
    <tr class="text-center">
      <td><?= $i ?></td>
      <td class="text-center"><?= $row['trailer_no']; ?></td>
      <td class="text-left"><?= $row['license_plate'] ?></td>
      <td class="text-left"><?= $row['province_name_th']; ?></td>
      <td class="text-left"><?= $row['model']; ?></td>
      <td class="text-left"><?= $row['employee_name']; ?></td>
      <td class="text-left"><?= $row['affiliation_name']; ?></td>
      <td class="text-left"><?= $row['department_name']; ?></td>
      <td>
        <button type="button" class="btn btn-warning btn-sm btn-flat" onclick="showForm(<?= $row['trailer_id']; ?>)">แก้ไข</button>
        <button type="button" class="btn btn-danger btn-sm btn-flat" onclick="removeRow(<?= $row['trailer_id']; ?>)">ลบ</button>
      </td>
    </tr>
<?php } ?>
</tbody>
</table>
<script>
$(function () {
  $('#tableDisplay').DataTable({
    'paging'      : true,
    'lengthChange': true,
    'aLengthMenu' : [[50, 100, -1], [50, 100,"All"]],
    'searching'   : true,
    'ordering'    : false,
    'info'        : true,
    'autoWidth'   : false
  })
})

</script>
