<?php

include('../../conf/connect.php');
include('../../inc/utils.php');
session_start();

$trailer_id          = $_POST['trailer_id'];
$trailer_no          = $_POST['trailer_no'];
$affiliation_id      = chkIdQury($_POST['affiliation_id']);
$license_plate       = $_POST['license_plate'];
$province_id         = chkIdQury($_POST['province_id']);
$trailer_brand_id    = chkIdQury($_POST['trailer_brand_id']);
$model               = $_POST['model'];
$car_serial_no       = $_POST['car_serial_no'];
$vin                 = $_POST['vin'];
$trailer_type_id     = chkIdQury($_POST['trailer_type_id']);
$trailer_category_id = chkIdQury($_POST['trailer_category_id']);
$drive_shaft         = $_POST['drive_shaft'];
$color               = $_POST['color'];
$weight              = $_POST['weight'];
$long_car            = $_POST['long_car'];
$nature              = $_POST['nature'];
$registration_date   = isset($_POST['registration_date'])?chkDateQury($_POST['registration_date']):'null';
$tax_expiration      = isset($_POST['tax_expiration'])?chkDateQury($_POST['tax_expiration']):'null';
$check_status        = $_POST['check_status'];
$gps                 = $_POST['gps'];
$employee_id         = chkIdQury($_POST['employee_id']);
$department_id       = chkIdQury($_POST['department_id']);


//$type                = $_POST['type'];
//$car_type_id         = $_POST['car_type_id'];
//$total_name          = $_POST['total_name'];


if($trailer_id != ""){
  $sql = "UPDATE tb_trailer SET
          trailer_no          = '$trailer_no',
          affiliation_id      = $affiliation_id,
          license_plate       = '$license_plate',
          province_id         = $province_id,
          trailer_brand_id    = $trailer_brand_id,
          model               = '$model',
          car_serial_no       = '$car_serial_no',
          vin                 = '$vin',
          trailer_type_id     = $trailer_type_id,
          trailer_category_id = $trailer_category_id,
          drive_shaft         = '$drive_shaft',
          color               = '$color',
          weight              = '$weight',
          long_car            = '$long_car',
          nature              = '$nature',
          registration_date   = $registration_date,
          tax_expiration      = $tax_expiration,
          check_status        = '$check_status',
          gps                 = '$gps',
          employee_id         = $employee_id,
          department_id       = $department_id
          WHERE trailer_id    = $trailer_id";
}else{
  $sql = "INSERT INTO  tb_trailer (
                                    trailer_no,
                                    affiliation_id,
                                    license_plate,
                                    province_id,
                                    trailer_brand_id,
                                    model,
                                    car_serial_no,
                                    vin,
                                    trailer_type_id,
                                    trailer_category_id,
                                    drive_shaft,
                                    color,
                                    weight,
                                    long_car,
                                    nature,
                                    registration_date,
                                    tax_expiration,
                                    check_status,
                                    gps,
                                    employee_id,
                                    department_id
                                  )
                                    VALUES('$trailer_no',
                                    $affiliation_id,
                                    '$license_plate',
                                    $province_id,
                                    $trailer_brand_id,
                                    '$model',
                                    '$car_serial_no',
                                    '$vin',
                                    $trailer_type_id,
                                    $trailer_category_id,
                                    '$drive_shaft',
                                    '$color',
                                    '$weight',
                                    '$long_car',
                                    '$nature',
                                    $registration_date,
                                    $tax_expiration,
                                    '$check_status',
                                    '$gps',
                                    $employee_id,
                                    $department_id)";
}

if(mysqli_query($conn,$sql)){
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'success','message' => 'success')));
}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'danger','message' => 'Fail :'.$sql)));
}
?>
