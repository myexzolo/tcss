<?php

include('../../conf/connect.php');
include('../../inc/utils.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
?>
<style>
.txtNum {
    text-align: right;
}
</style>
<table class="table table-bordered table-striped table-hover">
  <thead>
    <tr class="text-center">
      <th style="width:40px" class="text-center">No.</th>
      <th class="text-center">รหัสพนักงาน</th>
      <th class="text-center">ชื่อพนักงาน</th>
      <th class="text-center">ตำแหน่ง</th>
      <th class="text-center">โทรศัพท์</th>
      <th class="text-center">เงินเดือน</th>
    </tr>
  </thead>
  <tbody>
<?php
  $con = "";
  $employee_name       = isset($_POST['employeeName'])?$_POST['employeeName']:"";

  if($employee_name != ""){
    $con .= " and employee_name LIKE '%".$employee_name."%' ";
  }

  $num = 0;

  $sql = "SELECT * FROM  tb_employee_master where employee_id > 0 $con order by employee_no";
  //echo $sql;
  $query = mysqli_query($conn,$sql);
  $num = mysqli_num_rows($query);
  for ($i=1; $i <= $num ; $i++) {
    $row = mysqli_fetch_assoc($query);


    $employee_id    = $row['employee_id'];
    $employee_no    = $row['employee_no'];
    $employee_name  = $row['employee_name'];
    $position_id    = $row['position_id'];
    $address        = $row['address'];
    $tax            = $row['tax'];
    $address        = $row['address'];
    $account_no     = $row['account_no'];
    $tel            = $row['tel'];
    $department_id  = $row['department_id'];
    $hire_date      = $row['hire_date'];
    $status         = $row['status'];
    $salary         = chkNum($row['salary']);

    $optionPos  = getoptionPos($position_id);

?>
    <tr class="text-center" >
      <td><?= $i ?></td>
      <td class="text-center">
        <input value="<?= $employee_id ?>" name="employee_id[]" type="hidden">
        <input value="<?= $employee_no ?>" name="employee_no[]" type="text" class="form-control" autocomplete="off" placeholder="" required>
      </td>
      <td class="text-center">
        <input value="<?= $employee_name ?>" name="employee_name[]" type="text" class="form-control" autocomplete="off" placeholder="" required>
      </td>
      <td class="text-center" >
        <select name="position_id[]" class="form-control select2"  required >
          <option value="" ></option>
          <?= $optionPos ?>
        </select>
      </td>
      <td class="text-center">
        <input value="<?= $tel ?>" name="tel[]" type="text" class="form-control" autocomplete="off" placeholder="" >
      </td>
      <td class="text-center">
        <input value="<?= $salary; ?>" name="salary[]" type="text" class="form-control txtNum" data-smk-type="decimal" autocomplete="off" placeholder="" >
      </td>
    </tr>
<?php
}
?>
</tbody>
</table>
<?php
if($num > 0){
?>
  <div class="modal-footer">
    <button type="reset" class="btn btn-default  btn-flat" style="width:100px">ยกเลิก</button>
    <button type="submit" class="btn btn-primary btn-flat" style="width:100px">บันทึก</button>
  </div>
<?php
}
?>
<script>
  $(function () {
    $('#tableDisplay').DataTable({
      'paging'      : true,
      'lengthChange': true,
      'searching'   : false,
      'ordering'    : false,
      'info'        : true,
      'autoWidth'   : true
    })
  })

</script>
