<?php
include('../../conf/connect.php');
include('../../inc/utils.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
?>
<table class="table table-bordered table-striped" id="tableDisplay">
  <thead>
    <tr class="text-center">
      <td>Num</td>
      <td>User Login</td>
      <td>Name</td>
      <td>สถานะ</td>
      <td style="width:60px">Edit</td>
      <td style="width:60px">Del</td>
      <td style="width:100px">Re Password</td>
    </tr>
  </thead>
  <tbody>
<?php
  $sql = "SELECT * FROM t_user ORDER BY user_id DESC";
  $query = mysqli_query($conn,$sql);
  $num = mysqli_num_rows($query);
  for ($i=1; $i <= $num ; $i++) {
    $row = mysqli_fetch_assoc($query);
?>
    <tr class="text-center">
      <td><?= $i ?></td>
      <td><?= $row['user_login']; ?></td>
      <td><?= $row['user_name']; ?></td>
      <td><?= ($row['is_active'] = 'Y' ? 'ใช้งาน' : 'ไม่ใช้งาน') ; ?></td>
      <td>
        <button type="button" class="btn btn-warning btn-sm" onclick="showFormEdit(<?= $row['user_id'] ?>,'edit')">Edit</button>
      <td>
        <button type="button" class="btn btn-danger btn-sm" onclick="removeUser(<?= $row['user_id'] ?>)">Del</button>
      </td>
      <td>
        <button type="button" class="btn btn-sm" onclick="showFormEdit(<?= $row['user_id'] ?>,'resetPw')">Reset</button>
      </td>
    </tr>
<?php } ?>
</tbody>
</table>
<script>
  $(function () {
    $('#tableDisplay').DataTable();
  })
</script>
