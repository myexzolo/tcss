<?php
session_start();
include('../../conf/connect.php');
include('../../inc/utils.php');

$idJob          = isset($_POST['idJob'])?$_POST['idJob']:"";
$apinvoice_date = isset($_POST['apinvoice_date'])?$_POST['apinvoice_date']:"";
$credit         = isset($_POST['credit'])?chkNum($_POST['credit']):"0";
$maturity_date  = isset($_POST['maturity_date'])?chkDateQury($_POST['maturity_date']):'null';
$provision      = isset($_POST['provision'])?chkNum($_POST['provision']):"0";
$delivery_cost  = isset($_POST['delivery_cost'])?chkNum($_POST['delivery_cost']):"0";
$discount       = isset($_POST['discount'])?chkNum($_POST['discount']):"0";
$ot             = isset($_POST['ot'])?chkNum($_POST['ot']):"0";
$total_cost     = isset($_POST['total_cost'])?chkNum($_POST['total_cost']):"0";
$department_id  = isset($_POST['department_id'])?$_POST['department_id']:"";
$remark         = isset($_POST['remark'])?$_POST['remark']:"";

$apinvoice_no   = isset($_POST['apinvoice_no'])?$_POST['apinvoice_no']:"";

$installment   = isset($_POST['installment'])?chkNum($_POST['installment']):"0";//ค่างวดรถ
$oil_price     = isset($_POST['oil_price'])?chkNum($_POST['oil_price']):"0";//ค่าน้ำมัน
$gps           = isset($_POST['gps'])?chkNum($_POST['gps']):"0";
$sso           = isset($_POST['sso'])?chkNum($_POST['sso']):"0";//ประกันสังคม
$warranty      = isset($_POST['warranty'])?chkNum($_POST['warranty']):"0";//ประกันสินค้า
$insurance     = isset($_POST['insurance'])?chkNum($_POST['insurance']):"0";//ประกันภัยรถ
$register_head = isset($_POST['register_head'])?chkNum($_POST['register_head']):"0";//ต่อทะเบียนหัว
$register_footter = isset($_POST['register_footter'])?chkNum($_POST['register_footter']):"0";//ต่อทะเบียนหาง
$act           = isset($_POST['act'])?chkNum($_POST['act']):"0";//ต่อพรบ.
$tire          = isset($_POST['tire'])?chkNum($_POST['tire']):"0";//ค่ายาง
$other         = isset($_POST['other'])?chkNum($_POST['other']):"0";//อื่นๆ

$typeCompany = $_SESSION['typeCompany'];


if(!empty($idJob)){
  if($apinvoice_no != ""){
      $sql = "UPDATE tb_job_order SET apinvoice_no = null WHERE apinvoice_no  = '$apinvoice_no'";
      //echo $sql;
      mysqli_query($conn,$sql);

      $sql = "UPDATE tb_apinvoice SET
             credit = '$credit',
             maturity_date =$maturity_date,
             provision = '$provision',
             delivery_cost ='$delivery_cost',
             discount = '$discount',
             ot = '$ot',
             total_cost = '$total_cost',
             remark = '$remark',
             installment   = '$installment',
             oil_price     = '$oil_price',
             gps           = '$gps',
             sso           = '$sso',
             warranty      = '$warranty',
             insurance     = '$insurance',
             register_head = '$register_head',
             register_footter = '$register_footter',
             act           = '$act',
             tire          = '$tire',
             other         = '$other'
             where apinvoice_no = '$apinvoice_no'";
      mysqli_query($conn,$sql);

      //echo $sql;
  }else{
      $dateYM       = date('y-m-d');
      if($typeCompany == '1'){
        $ct           = "CT".$dateYM."-";
      }else{
        $ct           = "CV".$dateYM."-";
      }


      $sql = "SELECT max(apinvoice_no) as apinvoice_no FROM  tb_apinvoice where apinvoice_no LIKE '$ct%'";
      $query  = mysqli_query($conn,$sql);
      $row    = mysqli_fetch_assoc($query);
      $apinvoiceNo  = $row['apinvoice_no'];

      if(!empty($apinvoiceNo)){
        $lastNum = substr($apinvoiceNo,strlen($ct));
        $lastNum = $lastNum + 1;
        $apinvoice_no = $ct.sprintf("%03d", $lastNum);
      }else{
        $apinvoice_no = $ct.sprintf("%03d", 1);
      }

      $sql = "INSERT INTO tb_apinvoice
             (apinvoice_no,
              apinvoice_date,
              credit,
              maturity_date,
              provision,
              delivery_cost,
              discount,
              ot,
              total_cost,
              department_id,
              remark,
              installment,
              oil_price,
              gps,
              sso,
              warranty,
              insurance,
              register_head,
              register_footter,
              act,
              tire,
              other)
             VALUES(
               '$apinvoice_no',
               '$apinvoice_date',
               '$credit',
               $maturity_date,
               '$provision',
               '$delivery_cost',
               '$discount',
               '$ot',
               '$total_cost',
               '$department_id',
               '$remark',
               '$installment',
               '$oil_price',
               '$gps',
               '$sso',
               '$warranty',
               '$insurance',
               '$register_head',
               '$register_footter',
               '$act',
               '$tire',
               '$other'
             )";
      mysqli_query($conn,$sql);
      //echo $sql;
  }


  foreach( $idJob as $key => $id ) {
    $sql = "UPDATE tb_job_order SET apinvoice_no = '$apinvoice_no' WHERE id  = '$id'";
    mysqli_query($conn,$sql);
    //echo $sql;
  }
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'success','message' => 'success')));
}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'danger','message' => 'Fail')));
}



?>
