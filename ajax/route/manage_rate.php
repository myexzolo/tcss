<?php

include('../../conf/connect.php');
session_start();

$id                  = $_POST['id'];
$route_id            = $_POST['route_id'];
$rate                = $_POST['rate'];
$one_trip_ton        = $_POST['one_trip_ton'];
$price_per_trip      = $_POST['price_per_trip'];
$ext_one_trip_ton    = $_POST['ext_one_trip_ton'];
$ext_price_per_trip  = $_POST['ext_price_per_trip'];

$flag = true;
if(isset($route_id) && count($route_id) > 0){
  for($x= 0; $x < count($route_id); $x++){
    $ID                 = $id[$x];
    $routeId            = $route_id[$x];
    $rates              = $rate[$x];
    $oneTripTon         = isset($one_trip_ton[$x])?$one_trip_ton[$x]:"0";
    $pricePerTrip       = isset($price_per_trip[$x])?$price_per_trip[$x]:"0";
    $extOneTripTon      = isset($ext_one_trip_ton[$x])?$ext_one_trip_ton[$x]:"0";
    $extPricePerTrip    = isset($ext_price_per_trip[$x])?$ext_price_per_trip[$x]:"0";

    if($ID != ""){
      $sql = "UPDATE tb_route_price_rate SET
              one_trip_ton         = '$oneTripTon',
              price_per_trip       = '$pricePerTrip',
              ext_one_trip_ton     = '$extOneTripTon',
              ext_price_per_trip   = '$extPricePerTrip'
              WHERE id_route_rate  = '$ID'";
    }else{
      $sql = "INSERT INTO  tb_route_price_rate(
              rate,route_id,one_trip_ton,
              price_per_trip,ext_one_trip_ton,ext_price_per_trip
            )VALUES(
              '$rates','$routeId','$oneTripTon',
              '$pricePerTrip','$extOneTripTon','$extPricePerTrip')";
    }
    //echo $sql;
    $result = mysqli_query($conn,$sql);
  }
}

if($flag){
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'success','message' => 'success')));
}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'danger','message' => 'Fail :')));
}
?>
