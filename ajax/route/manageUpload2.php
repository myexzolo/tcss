<?php
require_once '../../Classes/PHPExcel.php';

include '../../Classes/PHPExcel/IOFactory.php';
include('../../conf/connect.php');
include("../../inc/utils.php");
session_start();


$target_file    = $_FILES["filepath"]["name"];
$inputFileName  = $target_file;

if (file_exists($target_file)) {
  unlink($target_file);
}


if (move_uploaded_file($_FILES["filepath"]["tmp_name"], $target_file)) {
    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objReader->setReadDataOnly(true);
    $objPHPExcel = $objReader->load($inputFileName);

    $objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
    $highestRow = $objWorksheet->getHighestRow();
    $highestColumn = $objWorksheet->getHighestColumn();

    $headingsArray = $objWorksheet->rangeToArray('A1:'.$highestColumn.'1',null, true, true, true);
    $headingsArray = $headingsArray[1];

    $headerName = $headingsArray['A'];

    $headingsArray = array("A" => "no",
                          "B" => "route_id",
                          "C" => "source",
                          "D" => "destination",
                          "E" => "distance1",
                          "F" => "distance2",
                          "G" => "allowance",
                          "H" => "allowance_oth",
                          "I" => "kog_expense",
                          "J" => "acc_expense",
                          "K" => "one_trip_ton_14",
                          "L" => "price_per_trip_14",
                          "M" => "ext_one_trip_ton_14",
                          "N" => "ext_price_per_trip_14",
                          "O" => "one_trip_ton_15",
                          "P" => "price_per_trip_15",
                          "Q" => "ext_one_trip_ton_15",
                          "R" => "ext_price_per_trip_15",
                          "S" => "one_trip_ton_16",
                          "T" => "price_per_trip_16",
                          "U" => "ext_one_trip_ton_16",
                          "V" => "ext_price_per_trip_16",
                          "W" => "one_trip_ton_17",
                          "X" => "price_per_trip_17",
                          "Y" => "ext_one_trip_ton_17",
                          "Z" => "ext_price_per_trip_17",
                          "AA" => "one_trip_ton_18",
                          "AB" => "price_per_trip_18",
                          "AC" => "ext_one_trip_ton_18",
                          "AD" => "ext_price_per_trip_18",
                          "AE" => "one_trip_ton_19",
                          "AF" => "price_per_trip_19",
                          "AG" => "ext_one_trip_ton_19",
                          "AH" => "ext_price_per_trip_19",
                          "AI" => "one_trip_ton_20",
                          "AJ" => "price_per_trip_20",
                          "AK" => "ext_one_trip_ton_20",
                          "AL" => "ext_price_per_trip_20",
                          "AM" => "one_trip_ton_21",
                          "AN" => "price_per_trip_21",
                          "AO" => "ext_one_trip_ton_21",
                          "AP" => "ext_price_per_trip_21",
                          "AQ" => "one_trip_ton_22",
                          "AR" => "price_per_trip_22",
                          "AS" => "ext_one_trip_ton_22",
                          "AT" => "ext_price_per_trip_22",
                          "AU" => "one_trip_ton_23",
                          "AV" => "price_per_trip_23",
                          "AW" => "ext_one_trip_ton_23",
                          "AX" => "ext_price_per_trip_23",
                          "AY" => "one_trip_ton_24",
                          "AZ" => "price_per_trip_24",
                          "BA" => "ext_one_trip_ton_24",
                          "BB" => "ext_price_per_trip_24",
                          "BC" => "one_trip_ton_25",
                          "BD" => "price_per_trip_25",
                          "BE" => "ext_one_trip_ton_25",
                          "BF" => "ext_price_per_trip_25",
                          "BG" => "one_trip_ton_26",
                          "BH" => "price_per_trip_26",
                          "BI" => "ext_one_trip_ton_26",
                          "BJ" => "ext_price_per_trip_26",
                          "BK" => "one_trip_ton_27",
                          "BL" => "price_per_trip_27",
                          "BM" => "ext_one_trip_ton_27",
                          "BN" => "ext_price_per_trip_27",
                          "BO" => "one_trip_ton_28",
                          "BP" => "price_per_trip_28",
                          "BQ" => "ext_one_trip_ton_28",
                          "BR" => "ext_price_per_trip_28",
                          "BS" => "one_trip_ton_29",
                          "BT" => "price_per_trip_29",
                          "BU" => "ext_one_trip_ton_29",
                          "BV" => "ext_price_per_trip_29",
                          "BW" => "one_trip_ton_30",
                          "BX" => "price_per_trip_30",
                          "BY" => "ext_one_trip_ton_30",
                          "BZ" => "ext_price_per_trip_30",
                          "CA" => "one_trip_ton_31",
                          "CB" => "price_per_trip_31",
                          "CC" => "ext_one_trip_ton_31",
                          "CD" => "ext_price_per_trip_31"
                         );
    $r = -1;

    $namedDataArray = array();
    for ($row = 2; $row <= $highestRow; ++$row) {
        $dataRow = $objWorksheet->rangeToArray('A'.$row.':'.$highestColumn.$row,null, true, true, true);
        if ((isset($dataRow[$row]['A'])) && ($dataRow[$row]['A'] > '')) {
            ++$r;
            foreach($headingsArray as $columnKey => $columnHeading) {
                $namedDataArray[$r][$columnHeading] = $dataRow[$row][$columnKey];
            }
        }
    }
    //
    $i = 0;
    foreach ($namedDataArray as $result) {
        if(trim($result["route_id"]) == ""){
          continue;
        }
        //echo "distance1 ".$result["distance1"];
        $no                       = trim($result["no"]);
        $route_id                 = trim($result["route_id"]);
        $source                   = trim($result["source"]);
        $destination              = trim($result["destination"]);
        $distance1                = isFloat(trim($result["distance1"]));
        $distance2                = isFloat(trim($result["distance2"]));
        $allowance                = isFloat(trim($result["allowance"]));
        $allowance_oth            = isFloat(trim($result["allowance_oth"]));
        $kog_expense              = isFloat(trim($result["kog_expense"]));
        $acc_expense              = isFloat(trim($result["acc_expense"]));
        $one_trip_ton[14]         = isFloat(trim($result["one_trip_ton_14"]));
        $price_per_trip[14]       = isFloat(trim($result["price_per_trip_14"]));
        $ext_one_trip_ton[14]     = isFloat(trim($result["ext_one_trip_ton_14"]));
        $ext_price_per_trip[14]   = isFloat(trim($result["ext_price_per_trip_14"]));
        $one_trip_ton[15]         = isFloat(trim($result["one_trip_ton_15"]));
        $price_per_trip[15]       = isFloat(trim($result["price_per_trip_15"]));
        $ext_one_trip_ton[15]     = isFloat(trim($result["ext_one_trip_ton_15"]));
        $ext_price_per_trip[15]   = isFloat(trim($result["ext_price_per_trip_15"]));
        $one_trip_ton[16]         = isFloat(trim($result["one_trip_ton_16"]));
        $price_per_trip[16]       = isFloat(trim($result["price_per_trip_16"]));
        $ext_one_trip_ton[16]     = isFloat(trim($result["ext_one_trip_ton_16"]));
        $ext_price_per_trip[16]   = isFloat(trim($result["ext_price_per_trip_16"]));
        $one_trip_ton[17]         = isFloat(trim($result["one_trip_ton_17"]));
        $price_per_trip[17]       = isFloat(trim($result["price_per_trip_17"]));
        $ext_one_trip_ton[17]     = isFloat(trim($result["ext_one_trip_ton_17"]));
        $ext_price_per_trip[17]   = isFloat(trim($result["ext_price_per_trip_17"]));
        $one_trip_ton[18]         = isFloat(trim($result["one_trip_ton_18"]));
        $price_per_trip[18]       = isFloat(trim($result["price_per_trip_18"]));
        $ext_one_trip_ton[18]     = isFloat(trim($result["ext_one_trip_ton_18"]));
        $ext_price_per_trip[18]   = isFloat(trim($result["ext_price_per_trip_18"]));
        $one_trip_ton[19]         = isFloat(trim($result["one_trip_ton_19"]));
        $price_per_trip[19]       = isFloat(trim($result["price_per_trip_19"]));
        $ext_one_trip_ton[19]     = isFloat(trim($result["ext_one_trip_ton_19"]));
        $ext_price_per_trip[19]   = isFloat(trim($result["ext_price_per_trip_19"]));
        $one_trip_ton[20]         = isFloat(trim($result["one_trip_ton_20"]));
        $price_per_trip[20]       = isFloat(trim($result["price_per_trip_20"]));
        $ext_one_trip_ton[20]     = isFloat(trim($result["ext_one_trip_ton_20"]));
        $ext_price_per_trip[20]   = isFloat(trim($result["ext_price_per_trip_20"]));
        $one_trip_ton[21]          = isFloat(trim($result["one_trip_ton_21"]));
        $price_per_trip[21]        = isFloat(trim($result["price_per_trip_21"]));
        $ext_one_trip_ton[21]      = isFloat(trim($result["ext_one_trip_ton_21"]));
        $ext_price_per_trip[21]   = isFloat(trim($result["ext_price_per_trip_21"]));
        $one_trip_ton[22]          = isFloat(trim($result["one_trip_ton_22"]));
        $price_per_trip[22]        = isFloat(trim($result["price_per_trip_22"]));
        $ext_one_trip_ton[22]      = isFloat(trim($result["ext_one_trip_ton_22"]));
        $ext_price_per_trip[22]   = isFloat(trim($result["ext_price_per_trip_22"]));
        $one_trip_ton[23]          = isFloat(trim($result["one_trip_ton_23"]));
        $price_per_trip[23]        = isFloat(trim($result["price_per_trip_23"]));
        $ext_one_trip_ton[23]      = isFloat(trim($result["ext_one_trip_ton_23"]));
        $ext_price_per_trip[23]    = isFloat(trim($result["ext_price_per_trip_23"]));
        $one_trip_ton[24]          = isFloat(trim($result["one_trip_ton_24"]));
        $price_per_trip[24]        = isFloat(trim($result["price_per_trip_24"]));
        $ext_one_trip_ton[24]      = isFloat(trim($result["ext_one_trip_ton_24"]));
        $ext_price_per_trip[24]    = isFloat(trim($result["ext_price_per_trip_24"]));
        $one_trip_ton[25]          = isFloat(trim($result["one_trip_ton_25"]));
        $price_per_trip[25]        = isFloat(trim($result["price_per_trip_25"]));
        $ext_one_trip_ton[25]      = isFloat(trim($result["ext_one_trip_ton_25"]));
        $ext_price_per_trip[25]    = isFloat(trim($result["ext_price_per_trip_25"]));
        $one_trip_ton[26]          = isFloat(trim($result["one_trip_ton_26"]));
        $price_per_trip[26]        = isFloat(trim($result["price_per_trip_26"]));
        $ext_one_trip_ton[26]      = isFloat(trim($result["ext_one_trip_ton_26"]));
        $ext_price_per_trip[26]    = isFloat(trim($result["ext_price_per_trip_26"]));
        $one_trip_ton[27]          = isFloat(trim($result["one_trip_ton_27"]));
        $price_per_trip[27]        = isFloat(trim($result["price_per_trip_27"]));
        $ext_one_trip_ton[27]      = isFloat(trim($result["ext_one_trip_ton_27"]));
        $ext_price_per_trip[27]    = isFloat(trim($result["ext_price_per_trip_27"]));
        $one_trip_ton[28]          = isFloat(trim($result["one_trip_ton_28"]));
        $price_per_trip[28]        = isFloat(trim($result["price_per_trip_28"]));
        $ext_one_trip_ton[28]      = isFloat(trim($result["ext_one_trip_ton_28"]));
        $ext_price_per_trip[28]    = isFloat(trim($result["ext_price_per_trip_28"]));
        $one_trip_ton[29]          = isFloat(trim($result["one_trip_ton_29"]));
        $price_per_trip[29]        = isFloat(trim($result["price_per_trip_29"]));
        $ext_one_trip_ton[29]      = isFloat(trim($result["ext_one_trip_ton_29"]));
        $ext_price_per_trip[29]    = isFloat(trim($result["ext_price_per_trip_29"]));
        $one_trip_ton[30]          = isFloat(trim($result["one_trip_ton_30"]));
        $price_per_trip[30]        = isFloat(trim($result["price_per_trip_30"]));
        $ext_one_trip_ton[30]      = isFloat(trim($result["ext_one_trip_ton_30"]));
        $ext_price_per_trip[30]    = isFloat(trim($result["ext_price_per_trip_30"]));
        $one_trip_ton[31]          = isFloat(trim($result["one_trip_ton_31"]));
        $price_per_trip[31]        = isFloat(trim($result["price_per_trip_31"]));
        $ext_one_trip_ton[31]      = isFloat(trim($result["ext_one_trip_ton_31"]));
        $ext_price_per_trip[31]    = isFloat(trim($result["ext_price_per_trip_31"]));

        $sql = "SELECT * FROM tb_route_price WHERE route_id = '$route_id'";
        $result    = mysqli_query($conn,$sql);
        $dataCount = mysqli_num_rows($result);

        //echo  ">>>>>>>>>.".$sql." ,".$dataCount;
        $strSQL = "";
        if($dataCount > 0){
            $strSQL = "UPDATE  tb_route_price SET
                  source        = '$source',
                  destination   = '$destination',
                  distance      = '$distance1',
                  allowance     = '$allowance',
                  allowance_oth = '$allowance_oth',
                  kog_expense   = '$kog_expense',
                  acc_expense   = '$acc_expense'
                  WHERE route_id   = '$route_id'";
        }else{

            $strSQL = "INSERT INTO  tb_route_price(
                    route_id,source,destination,
                    distance,allowance,allowance_oth,
                    kog_expense,acc_expense
                  )VALUES(
                    '$route_id','$source','$destination',
                    '$distance1','$allowance','$allowance_oth',
                    '$kog_expense','$acc_expense')";
            //echo  ">>>>>>>>> INSERT strSQL .".$strSQL;
        }

        mysqli_query($conn,$strSQL);


        for($rate = 14; $rate <= 31 ; $rate++)
        {
          $sql2 = "SELECT * FROM tb_route_price_rate WHERE route_id = '$route_id' and rate = $rate";
          $result2    = mysqli_query($conn,$sql2);
          $dataCount2 = mysqli_num_rows($result2);

          $strSQL = "";
          $oneTripTon       =  $one_trip_ton[$rate];
          $pricePerTrip     =  $price_per_trip[$rate];
          $extOneTripTon    =  $ext_one_trip_ton[$rate];
          $extPricePerTrip  =  $ext_price_per_trip[$rate];

          if($dataCount2 > 0){
              $strSQL = "UPDATE  tb_route_price_rate SET
                      one_trip_ton         = '$oneTripTon',
                      price_per_trip       = '$pricePerTrip',
                      ext_one_trip_ton     = '$extOneTripTon',
                      ext_price_per_trip   = '$extPricePerTrip'
                      WHERE rate = $rate and route_id  = '$route_id'";
             //echo $strSQL;
          }else{

              $strSQL = "INSERT INTO  tb_route_price_rate(
                      rate,route_id,one_trip_ton,
                      price_per_trip,ext_one_trip_ton,ext_price_per_trip
                    )VALUES(
                      '$rate','$route_id','$oneTripTon',
                      '$pricePerTrip','$extOneTripTon','$extPricePerTrip')";
         }
         mysqli_query($conn,$strSQL);
        }

  }
}

function isFloat($str){
  if(is_float($str) || is_numeric($str))
  {
    return $str;
  }else{
    return 0;
  }

}




?>
