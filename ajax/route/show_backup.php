<?php
include('../../conf/connect.php');

session_start();

header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$typeCompany =  $_SESSION['typeCompany'];

?>
<button type="button" class="btn btn-primary pull-left btn-flat" onclick="gotoPage('route_edit.php')"><i class="fa fa-edit"></i> แก้ไขค่าใช้จ่ายตามเส้นทาง</button>
<table class="table table-bordered table-striped table-hover" id="tableDisplay">
  <thead>
    <tr class="text-center">
      <th style="width:40px" class="text-center">No.</th>
      <th class="text-center">รหัสเส้นทาง </th>
      <th class="text-center">ต้นทาง</th>
      <th class="text-center">ปลายทาง</th>
      <th class="text-center">ระยะทาง</th>
      <th class="text-center">ตันละ</th>
      <th class="text-center">เที่ยวละ</th>
      <th class="text-center">เบี้ยเลี้ยง</th>
      <th class="text-center">อื่นๆ</th>
      <th class="text-center">คอก</th>
      <th class="text-center">คชจ.บัญชี</th>
      <th class="text-center">รถร่วม ตันละ</th>
      <th class="text-center">รถร่วม เที่ยวละ</th>
      <?php
      if($typeCompany == 2){
      ?>
        <th class="text-center">เลขที่สัญญา</th>
        <th class="text-center">ยอดค่าจ้าง</th>
      <?php
      }
      ?>


      <th style="width:100px;"></th>
    </tr>
  </thead>
  <tbody>
<?php
  $con = "";
  $source       = isset($_POST['source'])?$_POST['source']:"";
  $destination  = isset($_POST['destination'])?$_POST['destination']:"";

  if($source != ""){
    if($con == ""){
      $con = " where source LIKE '%".$source."%' ";
    }else{
      $con .= " and source LIKE '%".$source."%' ";
    }

  }

  if($destination != ""){
    if($con == ""){
      $con = " where destination LIKE '%".$destination."%'";
    }else{
      $con .= " and  destination LIKE '%".$destination."%'";
    }
  }

  $sql = "SELECT * FROM  tb_route_price $con order by source , destination";
  //echo $sql;
  $query = mysqli_query($conn,$sql);
  $num = mysqli_num_rows($query);
  for ($i=1; $i <= $num ; $i++) {
    $row = mysqli_fetch_assoc($query);
?>
    <tr class="text-center">
      <td><?= $i ?></td>
      <td class="text-center"><?= $row['route_id']; ?></td>
      <td class="text-left"><?= $row['source'] ?></td>
      <td class="text-left"><?= $row['destination']; ?></td>
      <td class="text-right fontTHSarabun"><?= $row['distance']; ?></td>
      <td class="text-right fontTHSarabun"><?= $row['one_trip_ton']; ?></td>
      <td class="text-right fontTHSarabun"><?= number_format($row['price_per_trip'],2); ?></td>
      <td class="text-right fontTHSarabun"><?= number_format($row['allowance'],2); ?></td>
      <td class="text-right fontTHSarabun"><?= number_format($row['allowance_oth'],2); ?></td>
      <td class="text-right fontTHSarabun"><?= number_format($row['kog_expense'],2); ?></td>
      <td class="text-right fontTHSarabun"><?= number_format($row['acc_expense'],2); ?></td>
      <td class="text-right fontTHSarabun"><?= number_format($row['ext_one_trip_ton'],2); ?></td>
      <td class="text-right fontTHSarabun"><?= number_format($row['ext_price_per_trip'],2); ?></td>
      <?php
      if($typeCompany == 2){
      ?>
        <td class="text-center fontTHSarabun"><?= $row['contract_no']; ?></td>
        <td class="text-right fontTHSarabun"><?= number_format($row['price_contract'],2); ?></td>
      <?php
      }
      ?>
      <td>
        <button type="button" class="btn btn-warning btn-sm btn-flat" onclick="showForm(<?= $row['id']; ?>)">แก้ไข</button>
        <button type="button" class="btn btn-danger btn-sm btn-flat" onclick="removeRow(<?= $row['id']; ?>)">ลบ</button>
      </td>
    </tr>
<?php } ?>
</tbody>
</table>
<script>
  $(function () {
    $('#tableDisplay').DataTable({
      'paging'      : true,
      'lengthChange': true,
      'aLengthMenu' : [[50, 100, -1], [50, 100,"All"]],
      'searching'   : false,
      'ordering'    : false,
      'info'        : true,
      'autoWidth'   : false
    })
  })

</script>
