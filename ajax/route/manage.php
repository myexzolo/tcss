<?php

include('../../conf/connect.php');
session_start();

$id                  = $_POST['id'];
$route_id            = $_POST['route_id'];
$cust_id             = !empty($_POST['cust_id'])?$_POST['cust_id']:"0";
$source              = $_POST['source'];
$destination         = $_POST['destination'];
$distance            = !empty($_POST['distance'])?$_POST['distance']:"0";
$one_trip_ton        = !empty($_POST['one_trip_ton'])?$_POST['one_trip_ton']:"0";
//$round_trip_ton      = $_POST['round_trip_ton'];
$round_trip_ton      = 0;
$price_per_trip      = $_POST['price_per_trip'];
$allowance           = !empty($_POST['allowance'])?$_POST['allowance']:"0";
$allowance_oth       = !empty($_POST['allowance_oth'])?$_POST['allowance_oth']:"0";
$kog_expense         = !empty($_POST['kog_expense'])?$_POST['kog_expense']:"0";
$acc_expense         = !empty($_POST['acc_expense'])?$_POST['acc_expense']:"0";
$ext_one_trip_ton    = !empty($_POST['ext_one_trip_ton'])?$_POST['ext_one_trip_ton']:"0";
//$ext_round_trip_ton  = $_POST['ext_round_trip_ton'];
$ext_round_trip_ton   = 0;
$ext_price_per_trip   = !empty($_POST['ext_price_per_trip'])?$_POST['ext_price_per_trip']:"0";
$contract_no          = isset($_POST['contract_no'])?$_POST['contract_no']:"";
$price_contract       = isset($_POST['price_contract'])?$_POST['price_contract']:"0";
$fuel_litre_total     = isset($_POST['fuel_litre_total'])?$_POST['fuel_litre_total']:"0";
$contract_detail      = isset($_POST['contract_detail'])?$_POST['contract_detail']:"";

if($id != ""){
  $sql = "UPDATE tb_route_price SET
          route_id            = '$route_id',
          cust_id             = '$cust_id',
          source              = '$source',
          destination         = '$destination',
          distance            = '$distance',
          one_trip_ton        = '$one_trip_ton',
          round_trip_ton      = '$round_trip_ton',
          price_per_trip      = '$price_per_trip',
          allowance           = '$allowance',
          allowance_oth       = '$allowance_oth',
          kog_expense         = '$kog_expense',
          acc_expense         = '$acc_expense',
          ext_one_trip_ton    = '$ext_one_trip_ton',
          ext_round_trip_ton  = '$ext_round_trip_ton',
          ext_price_per_trip  = '$ext_price_per_trip',
          contract_no         = '$contract_no',
          contract_detail     = '$contract_detail',
          price_contract      = '$price_contract',
          fuel_litre_total    = '$fuel_litre_total'
          WHERE id  = '$id'";
}else{
  $sql = "INSERT INTO tb_route_price (route_id, cust_id, source, destination, distance, one_trip_ton, round_trip_ton,
                      price_per_trip, allowance, allowance_oth, kog_expense, acc_expense, ext_one_trip_ton,
                      ext_round_trip_ton, ext_price_per_trip, contract_no, price_contract, fuel_litre_total,contract_detail)
              VALUES ('$route_id', '$cust_id', '$source', '$destination', '$distance','$one_trip_ton', '$round_trip_ton',
                      '$price_per_trip', '$allowance', '$allowance_oth', '$kog_expense','$acc_expense', '$ext_one_trip_ton',
                      '$ext_round_trip_ton', '$ext_price_per_trip','$contract_no','$price_contract','$fuel_litre_total','$contract_detail')";
}

//echo $sql;

if(mysqli_query($conn,$sql)){
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'success','message' => 'success')));
}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'danger','message' => 'Fail :'.$sql)));
}
?>
