<?php

include('../../conf/connect.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$strExcelFileName="export_route.xls";

header("Content-Type: application/x-msexcel; name=\"$strExcelFileName\"");
header("Content-Disposition: inline; filename=\"$strExcelFileName\"");
header("Pragma:no-cache");
?>
<html xmlns:o="urn:schemas-microsoft-com:office:office"xmlns:x="urn:schemas-microsoft-com:office:excel"xmlns="http://www.w3.org/TR/REC-html40">

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
<div id="SiXhEaD_Excel" align=center x:publishsource="Excel">
<table x:str border=1 cellpadding=0 cellspacing=1 width=100% style="border-collapse:collapse">
  <thead>
    <tr class="text-center">
      <th colspan="13"  class="text-center">รายการค่าใช้จ่ายตามเส้นทาง</th>
    </tr>
    <tr class="text-center">
      <th class="text-center">No.</th>
      <th class="text-center">รหัสเส้นทาง </th>
      <th class="text-center">ต้นทาง</th>
      <th class="text-center">ปลายทาง</th>
      <th class="text-center">ระยะทาง</th>
      <th class="text-center">ตันละ</th>
      <th class="text-center">เที่ยวละ</th>
      <th class="text-center">เบี้ยเลี้ยง</th>
      <th class="text-center">อื่นๆ</th>
      <th class="text-center">คอก</th>
      <th class="text-center">คชจ.บัญชี</th>
      <th class="text-center">รถร่วม ตันละ</th>
      <th class="text-center">รถร่วม เที่ยวละ</th>
    </tr>
  </thead>
  <tbody>
<?php
  $con = "";
  $source       = isset($_GET['source'])?$_GET['source']:"";
  $destination  = isset($_GET['destination'])?$_GET['destination']:"";

  if($source != ""){
    if($con == ""){
      $con = " where source LIKE '%".$source."%' ";
    }else{
      $con .= " and source LIKE '%".$source."%' ";
    }

  }

  if($destination != ""){
    if($con == ""){
      $con = " where destination LIKE '%".$destination."%'";
    }else{
      $con .= " and  destination LIKE '%".$destination."%'";
    }
  }

  $sql = "SELECT * FROM  tb_route_price $con order by source , destination";
  //echo $sql;
  $query = mysqli_query($conn,$sql);
  $num = mysqli_num_rows($query);
  for ($i=1; $i <= $num ; $i++) {
    $row = mysqli_fetch_assoc($query);
?>
    <tr class="text-center">
      <td><?= $i ?></td>
      <td class="text-center"><?= $row['route_id']; ?></td>
      <td class="text-left"><?= $row['source'] ?></td>
      <td class="text-left"><?= $row['destination']; ?></td>
      <td class="text-right"><?= $row['distance']; ?></td>
      <td class="text-right"><?= $row['one_trip_ton']; ?></td>
      <td class="text-right"><?= $row['price_per_trip']; ?></td>
      <td class="text-right"><?= $row['allowance']; ?></td>
      <td class="text-right"><?= $row['allowance_oth']; ?></td>
      <td class="text-right"><?= $row['kog_expense']; ?></td>
      <td class="text-right"><?= $row['acc_expense']; ?></td>
      <td class="text-right"><?= $row['ext_one_trip_ton']; ?></td>
      <td class="text-right"><?= $row['ext_price_per_trip']; ?></td>
    </tr>
<?php } ?>
</tbody>
</table>
<script>
window.onbeforeunload = function(){return false;};
setTimeout(function(){window.close();}, 10000);
</script>
</body>
</html>
