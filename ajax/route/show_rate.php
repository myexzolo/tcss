<?php

include('../../conf/connect.php');
include('../../inc/utils.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
?>
<style>
.txtNum {
    text-align: right;
    font-size:14px;
}
</style>
<table class="table table-bordered table-striped table-hover" style="font-size:14px;">
  <thead>
    <tr class="text-center">
      <th style="width:40px" class="text-center">No.</th>
      <th style="width:120px" class="text-center">รหัสเส้นทาง </th>
      <th class="text-center">ต้นทาง</th>
      <th class="text-center">ปลายทาง</th>
      <th style="width:140px" class="text-center">Rate ราคา</th>
      <th style="width:140px" class="text-center">ตันละ</th>
      <th style="width:140px" class="text-center">เที่ยวละ</th>
      <th style="width:140px" class="text-center">รถร่วม ตันละ</th>
      <th style="width:140px" class="text-center">รถร่วม เที่ยวละ</th>
    </tr>
  </thead>
  <tbody>
<?php
  $flag = false;
  $routeId    = isset($_POST['route_id'])?$_POST['route_id']:"";


  $sql = "SELECT r.*,rp.source, rp.destination
          FROM tb_route_price rp
          LEFT JOIN tb_route_price_rate r ON rp.route_id = r.route_id
          where rp.route_id = '$routeId'
          order by r.rate";
  //echo  $sql;
  $query = mysqli_query($conn,$sql);
  $num = mysqli_num_rows($query);
  $nums= 0;
  if($num > 0){
    for ($i=1; $i <= $num ; $i++) {
      $row = mysqli_fetch_assoc($query);

      $id                 = $row['id_route_rate'];
      $rate               = $row['rate'];
      $route_id           = $row['route_id'];
      $source             = $row['source'];
      $destination        = $row['destination'];
      $one_trip_ton       = intval(chkNum($row['one_trip_ton']));
      $price_per_trip     = intval(chkNum($row['price_per_trip']));
      $ext_one_trip_ton   = intval(chkNum($row['ext_one_trip_ton']));
      $ext_price_per_trip = intval(chkNum($row['ext_price_per_trip']));


      if($rate == "20" && $i == 1)
      {
        for ($y=14; $y <= 19 ; $y++)
        {
          $nums++;
 ?>
         <tr class="text-center" >
           <td>
             <?= $nums ?>
             <input value="" name="id[]" type="hidden">
             <input value="<?= $routeId; ?>" name="route_id[]" type="hidden" >
             <input value="<?= $y; ?>" name="rate[]" type="hidden" >
           </td>
           <td class="text-center"><?= $routeId; ?></td>
           <td class="text-left"><?= $source ?></td>
           <td class="text-left"><?= $destination; ?></td>
           <td class="text-center">
             <?= $y; ?>
           </td>
           </td>
           <td class="text-center">
             <input value="0" name="one_trip_ton[]" type="number" class="form-control txtNum" autocomplete="off" placeholder="" >
           </td>
           <td class="text-center">
             <input value="0" name="price_per_trip[]" type="number" class="form-control txtNum" autocomplete="off" placeholder="" >
           </td>
           <td class="text-center">
             <input value="0" name="ext_one_trip_ton[]" type="number" class="form-control txtNum" autocomplete="off" placeholder="" >
           </td>
           <td class="text-center">
             <input value="0" name="ext_price_per_trip[]" type="number" class="form-control txtNum" autocomplete="off"  placeholder="" >
           </td>
         </tr>
 <?php
        }
      }

      if($rate == "" && $num == 1)
      {
        $flag = true;
        break;
      }
  ?>
      <tr class="text-center" >
        <td>
          <?= $nums+$i ?>
          <input value="<?= $id; ?>" name="id[]" type="hidden">
          <input value="<?= $route_id; ?>" name="route_id[]" type="hidden" >
          <input value="<?= $rate; ?>" name="rate[]" type="hidden" >
        </td>
        <td class="text-center"><?= $route_id; ?></td>
        <td class="text-left"><?= $source ?></td>
        <td class="text-left"><?= $destination; ?></td>
        <td class="text-center">
          <?= $rate; ?>
        </td>
        </td>
        <td class="text-center">
          <input value="<?= $one_trip_ton; ?>" name="one_trip_ton[]" type="number" class="form-control txtNum" autocomplete="off" placeholder="" >
        </td>
        <td class="text-center">
          <input value="<?= $price_per_trip; ?>" name="price_per_trip[]" type="number" class="form-control txtNum" autocomplete="off" placeholder="" >
        </td>
        <td class="text-center">
          <input value="<?= $ext_one_trip_ton; ?>" name="ext_one_trip_ton[]" type="number" class="form-control txtNum" autocomplete="off" placeholder="" >
        </td>
        <td class="text-center">
          <input value="<?= $ext_price_per_trip; ?>" name="ext_price_per_trip[]" type="number" class="form-control txtNum" autocomplete="off"  placeholder="" >
        </td>
      </tr>
  <?php
  }
}

if($flag){
  $num= 0;
  for ($i=14; $i <= 31 ; $i++)
  {
    $num++;
?>
<tr class="text-center" >
  <td>
    <?= $num ?>
    <input value="" name="id[]" type="hidden">
    <input value="<?= $routeId; ?>" name="route_id[]" type="hidden" >
    <input value="<?= $i; ?>" name="rate[]" type="hidden" >
  </td>
  <td class="text-center"><?= $routeId; ?></td>
  <td class="text-left"><?= $source ?></td>
  <td class="text-left"><?= $destination; ?></td>
  <td class="text-center">
    <?= $i; ?>
  </td>
  </td>
  <td class="text-center">
    <input value="0" name="one_trip_ton[]" type="number" class="form-control txtNum" autocomplete="off" placeholder="" >
  </td>
  <td class="text-center">
    <input value="0" name="price_per_trip[]" type="number" class="form-control txtNum" autocomplete="off" placeholder="" >
  </td>
  <td class="text-center">
    <input value="0" name="ext_one_trip_ton[]" type="number" class="form-control txtNum" autocomplete="off" placeholder="" >
  </td>
  <td class="text-center">
    <input value="0" name="ext_price_per_trip[]" type="number" class="form-control txtNum" autocomplete="off"  placeholder="" >
  </td>
</tr>

<?php
  }
}
?>
</tbody>
</table>
<div class="modal-footer">
  <button type="button" class="btn btn-success btn-flat" style="width:100px" onclick="gotoPage('route.php')">กลับ</button>
  <button type="reset" class="btn btn-default  btn-flat" style="width:100px">ยกเลิก</button>
  <button type="submit" class="btn btn-primary btn-flat" style="width:100px">บันทึก</button>
</div>
<script>
  $(function () {
    $('#tableDisplay').DataTable({
      'paging'      : true,
      'lengthChange': true,
      'searching'   : false,
      'ordering'    : false,
      'info'        : true,
      'autoWidth'   : true
    })
  })

</script>
