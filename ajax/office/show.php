<?php

include('../../conf/connect.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
?>
<table class="table table-bordered table-striped table-hover" id="tableDisplay">
  <thead>
    <tr class="text-center">
      <th style="width:40px" class="text-center">No.</th>
      <th class="text-center">รหัสบริษัท</th>
      <th class="text-center">ชื่อบริษัท</th>
      <th class="text-center">ผู้ติดต่อ</th>
      <th class="text-center">เบอร์โทร</th>
      <th class="text-center">เลขที่ประจำตัวผู้เสียภาษี</th>
      <th style="width:100px;"></th>
    </tr>
  </thead>
  <tbody>
<?php
  $sql = "SELECT * FROM tb_company_master";

  $query = mysqli_query($conn,$sql);
  $num = mysqli_num_rows($query);
  for ($i=1; $i <= $num ; $i++) {
    $row = mysqli_fetch_assoc($query);
?>
    <tr class="text-center">
      <td><?= $i ?></td>
      <td class="text-left"><?= $row['company_no']; ?></td>
      <td class="text-left"><?= $row['company_name']; ?></td>
      <td class="text-left"><?= $row['contact']; ?></td>
      <td class="text-left"><?= $row['tel']; ?></td>
      <td class="text-left"><?= $row['tax']; ?></td>
      <td>
        <button type="button" class="btn btn-warning btn-sm btn-flat" onclick="showForm(<?= $row['company_id']; ?>)">แก้ไข</button>
        <button type="button" class="btn btn-danger btn-sm btn-flat" onclick="removeRow(<?= $row['company_id']; ?>)">ลบ</button>
      </td>
    </tr>
<?php } ?>
</tbody>
</table>
<script>
  $(function () {
    $('#tableDisplay').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : true,
      'ordering'    : false,
      'info'        : true,
      'autoWidth'   : false
    })
  })

</script>
