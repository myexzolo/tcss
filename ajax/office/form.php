<?php

include('../../conf/connect.php');
session_start();

header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
?>

<?php
$Company_No   = "";
$Company_Name = "";
$Tax          = "";
$Address      = "";
$Postal_Code  = "";
$Contact      = "";
$Tel          = "";
$Fax          = "";
$Email = "";

$Company_Id = isset($_POST['value'])?$_POST['value']:"";

if(!empty($Company_Id))
{
    $sql = "SELECT * FROM tb_company_master WHERE company_id = '$Company_Id'";
    $query = mysqli_query($conn,$sql);
    $row = mysqli_fetch_assoc($query);

    $Company_Id   = $row['company_id'];
    $Company_No   = $row['company_no'];
    $Company_Name = $row['company_name'];
    $Tax          = $row['tax'];
    $Address      = $row['address'];
    $Postal_Code  = $row['postal_code'];
    $Contact      = $row['contact'];
    $Tel          = $row['tel'];
    $Fax          = $row['fax'];
    $Email        = $row['email'];
}
?>
<div class="row">
  <div class="col-md-3">
    <div class="form-group">
      <label>รหัสบริษัท</label>
      <input value="<?= $Company_No ?>" name="Company_No" type="text" class="form-control" placeholder="รหัสบริษัท" required>
    </div>
  </div>
  <div class="col-md-6">
    <div class="form-group">
      <label>ชื่อบริษัท</label>
      <input value="<?= $Company_Name ?>" name="Company_Name" type="text" class="form-control" placeholder="ชื่อบริษัท" required>
    </div>
  </div>
  <div class="col-md-3">
    <div class="form-group">
      <label>เลขที่ประจำตัวผู้เสียภาษี</label>
      <input value="<?= $Tax ?>" name="Tax" type="text" class="form-control" placeholder="Tax" >
    </div>
  </div>
  <div class="col-md-9">
    <div class="form-group">
      <label>ที่อยู่</label>
      <textarea class="form-control" name="Address" id="Address" rows="2" placeholder="Address ..." required><?= $Address ?></textarea>
    </div>
  </div>
  <div class="col-md-3">
    <div class="form-group">
      <label>รหัสไปรษณีย์</label>
      <input value="<?= $Postal_Code ?>" name="Postal_Code" type="text" class="form-control" placeholder="รหัสไปรษณีย์" >
    </div>
  </div>
  <div class="col-md-6">
    <div class="form-group">
      <label>ผู้ติดต่อ</label>
      <input value="<?= $Contact ?>" name="Contact" type="text" class="form-control" placeholder="ผู้ติดต่อ" >
    </div>
  </div>
  <div class="col-md-3">
    <div class="form-group">
      <label>โทรศัพท์</label>
      <input value="<?= $Tel ?>" name="Tel" type="text" class="form-control" placeholder="Phone" >
    </div>
  </div>
  <div class="col-md-3">
    <div class="form-group">
      <label>Fax</label>
      <input value="<?= $Fax ?>" name="Fax" type="text" class="form-control" placeholder="Fax" >
    </div>
  </div>
  <div class="col-md-6">
    <div class="form-group">
      <label>Email</label>
      <input value="<?= $Email ?>" name="Email" type="email" class="form-control" placeholder="Email" >
    </div>
  </div>


</div>

<input  type="hidden" value="<?= $Company_Id ?>" name="companyId" type="text" >
