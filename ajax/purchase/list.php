<?php
session_start();
include('../../conf/connect.php');
include('../../inc/utils.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$departmentId   = isset($_POST['departmentId'])?$_POST['departmentId']:"";
$startDate      = isset($_POST['startDate'])?$_POST['startDate']:"";
$endDate        = isset($_POST['endDate'])?$_POST['endDate']:"";
$numRow = 0;
$con = "";

if($startDate != "" and  $endDate != "")
{
  $con .= " and pv.date_start BETWEEN  '". $startDate ."' and '". $endDate ."'";
}
if($departmentId != ""){

  $con .= " and pv.department_id  ='". $departmentId ."' ";
}

?>
<input type="hidden" id="typeCompany" value="<?= $_SESSION['typeCompany'] ?>">

<table class="table table-bordered table-striped" id="tableDisplay">
  <thead>
    <tr class="text-center">
      <th class="text-center"style="width:40px;">ลำดับ</th>
      <th class="text-center">เลขที่ใบสำคัญจ่าย</th>
      <th class="text-center">ชื่อเจ้าของรถร่วม</th>
      <th class="text-center" >วันที่เริ่ม</th>
      <th class="text-center" >วันที่สิ้นสุด</th>
      <th class="text-center" >รายการหัก</th>
      <th class="text-center" >รายจ่ายสุทธิ</th>
      <th style="width:210px;"></th>
      <th style="width:50px;"></th>
    </tr>
  </thead>
  <tbody>
<?php

  $sql = "SELECT pv.*,d.department_name FROM  tb_pv pv,  tb_department_master d
  where pv.department_id = d.department_id and pv.status_del <> 'Y' $con order by pv_code DESC";
  //echo $sql;
  $query  = mysqli_query($conn,$sql);
  $num    = mysqli_num_rows($query);

  for ($i=1; $i <= $num ; $i++) {
    $row = mysqli_fetch_assoc($query);
    $pv_id              = $row['pv_id'];
    $pv_code            = $row['pv_code'];
    $date_start         = formatDate($row['date_start'],'d/m/Y');
    $date_end           = formatDate($row['date_end'],'d/m/Y');
    $department_id      = $row['department_id'];//รหัสลูกค้า
    $department_name    = $row['department_name'];//รหัสลูกค้า
    $deductions         = $row['deductions'];
    $pv_total           = $row['pv_total'];

?>
  <tr class="text-center">
    <td class="text-center"><?= $i ?></td>
    <td class="text-center"><?= $pv_code ?></td>
    <td class="text-left"><?= $department_name ?></td>
    <td class="text-center"><?= $date_start ?></td>
    <td class="text-center"><?= $date_end ?></td>
    <td class="text-right" ><?= number_format($deductions,2); ?></td>
    <td class="text-right" ><?= number_format($pv_total,2); ?></td>
    <td>
      <button type="button" class="btn btn-warning btn-sm btn-flat" onclick="editPurchase('<?= $pv_code; ?>','<?= $department_id; ?>')" ><i class="fa fa-edit"></i> แก้ไข</button>
      <button type="button" class="btn btn-primary btn-sm btn-flat" onclick="exportExcel('<?= $pv_code; ?>','<?= $department_id; ?>')"><i class="fa fa-file-excel-o"></i> Excel</button>
      <button type="button" class="btn btn-info btn-sm btn-flat" onclick="expPurchase('<?= $pv_code; ?>')"><i class="fa fa-print"></i> พิมพ์</button>
    </td>
    <td>
      <button type="button" class="btn btn-danger btn-sm btn-flat" onclick="delPurchase('<?= $pv_code; ?>')" ><i class="fa fa-ban"></i> ยกเลิก</button>
    </td>
  </tr>

<?php } ?>
</tbody>
</table>
<script>
  $(function () {
    $('#tableDisplay').DataTable({
     'paging'      : false,
     'lengthMenu'  : [50, 100, 150,200],
     'lengthChange': false,
     'searching'   : false,
     'ordering'    : false,
     'info'        : false,
     'autoWidth'   : false
   })
  })
</script>
