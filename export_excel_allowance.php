<?php
include('conf/connect.php');
include('inc/utils.php');


$startDate    = isset($_GET['startDate'])?$_GET['startDate']:"";
$endDate      = isset($_GET['endDate'])?$_GET['endDate']:"";
$periodCode   = isset($_GET['periodCode'])?$_GET['periodCode']:"";
$strExcelFileName="export_allowance_".$periodCode.".xls";

header("Content-Type: application/x-msexcel; name=\"$strExcelFileName\"");
header("Content-Disposition: inline; filename=\"$strExcelFileName\"");
header("Pragma:no-cache");

$numRow = 0;
$con = "";

// if($startDate != "" and  $endDate != "")
// {
//   $con .= " and jo.job_order_date BETWEEN  '". $startDate ."' and '". $endDate ."'";
// }

if($periodCode != ""){
  $con .= " and jo.period_code =  '". $periodCode ."'";
}

?>
<html xmlns:o="urn:schemas-microsoft-com:office:office"xmlns:x="urn:schemas-microsoft-com:office:excel"xmlns="http://www.w3.org/TR/REC-html40">

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
<div id="SiXhEaD_Excel" align=center x:publishsource="Excel">
  <div align="center" style="font-size:14px;"><b>รายงานโอนเงินเบี้ยเลี้ยง</b></div>
  <div align="center" style="font-size:14px;">
    <b>
      งวดที่ <?=$periodCode;?>&nbsp;
      วันที่ <?= urldecode($startDate);?>
    </b>
  </div>
  <table x:str border=1 cellpadding=0 cellspacing=1 width=100% style="border-collapse:collapse">
    <thead>
      <tr>
        <th rowspan="2" width="50" valign="middle" >ลำดับ</th>
        <th rowspan="2" width="150" valign="middle">ชื่อพนักงานขับรถ</th>
        <th rowspan="2" width="90" valign="middle">ทะเบียนรถ</th>
        <th rowspan="2" width="90" valign="middle">ระยะทาง<br>(กม.)</th>
        <th rowspan="2" width="90" valign="middle">จำนวน<br>ลิตร</th>
        <th colspan="5" >รายได้สุทธิ</th>
        <th rowspan="2" width="100" valign="middle">ยอดเงินโอน</th>
        <th rowspan="2" width="100" valign="middle">เลขที่เอกสาร<br>(DP)</th>
        <th rowspan="2" width="200" valign="middle">ต้นทาง</th>
        <th rowspan="2" width="200" valign="middle">ปลายทาง</th>
        <th rowspan="2" width="250" valign="middle">หมายเหตุ</th>
      </tr>
      <tr>
        <th width="70">น้ำมัน</th>
        <th width="70">เบี้ยเลี้ยง</th>
        <th width="70">คอก</th>
        <th width="70">ตีเปล่า</th>
        <th width="70">อื่นๆ</th>
      </tr>
    </thead>
    <tbody>
      <?php
        //$sql = "SELECT * FROM tb_po_customer pc, tb_customer_master cm, tb_employee_master em where pc.Cust_ID = cm.Cust_ID and pc.Employee_No = em.Employee_No $con ";
        $sql = "SELECT jo.*,em.employee_name,t.license_plate
        FROM tb_job_order jo, tb_employee_master em, tb_trailer t
        where jo.employee_id = em.employee_id $con and  jo.trailer_id = t.trailer_id and  jo.job_status_id = '2'
        order by jo.job_order_date";
        //echo $sql;
        $query  = mysqli_query($conn,$sql);
        $num = mysqli_num_rows($query);

        $totalTransfer = 0;
        $fuel_litres      = 0;
        $fuel_costs       = 0;
        $allowances       = 0;
        $kog_expenses     = 0;
        $blank_charges    = 0;
        $allowance_oths   = 0;
        $j = 0;
        for ($i=1; $i <= $num ; $i++) {
          $row = mysqli_fetch_assoc($query);
          $Employee_Name      = $row['employee_name'];
          $license_plate      = $row['license_plate'];
          $id                 = $row['id'];
          $job_order_no       = $row['job_order_no'];
          $delivery_type_id   = $row['delivery_type_id'];//รูปแบบการขนส่ง
          $job_order_date     = formatDate($row['job_order_date'],'d/m/Y');//วันที่ออกใบสั่งงาน
          $job_delivery_date  = formatDate($row['job_delivery_date'],'d/m/Y');;//วันที่กำหนดส่งสินค้า
          $cust_id            = $row['cust_id'];//รหัสลูกค้า
          $employee_id        = $row['employee_id'];//รหัสพนักงาน
          $employee_id2       = @$row['employee_id2'];//รหัสพนักงาน
          $second_emp         = @$row['second_emp'];//รหัสพนักงาน
          $trailer_id         = $row['trailer_id'];//หัว หมายเลขทะเบียนรถ
          $away_regi_no       = $row['away_regi_no'];//หาง
          $away_spec_no       = $row['away_spec_no'];//สเปคหาง
          $affiliation_id     = $row['affiliation_id'];//รหัส รถ(สังกัด)
          $route_id           = $row['route_id'];//รหัสเส้นทาง
          $source             = $row['source'];//ต้นทาง
          $destination        = $row['destination'];//ปลายทาง
          $distance           = $row['distance'];//ระยะทาง
          $cust_dp            = $row['cust_dp'];//DP
          $delivery_doc       = $row['delivery_doc'];//ใบส่งของ
          $department         = $row['department'];//หน่วยงาน
          $product_name       = $row['product_name'];//สินค้า
          $number_of_trips    = $row['number_of_trips'];//จำนวนเที่ยว
          $weights            = $row['weights'];//น้ำหนัก(ตัน)
          $shipping_amount    = $row['shipping_amount'];//ราคาค่าขนส่ง
          $date_product_in    = $row['date_product_in'];//วันทีรับสินค้า
          $time_product_in    = $row['time_product_in'];//เวลารับสินค้า
          $date_product_out   = $row['date_product_out'];//วันที่ออกสินค้า
          $time_product_out   = $row['time_product_out'];//เวลาออกสินค้า
          $date_delivery_in   = $row['date_delivery_in'];//วันทีส่งสินค้า
          $time_delivery_in   = $row['time_delivery_in'];//เวลาเข้าส่งสินค้า
          $date_delivery_out  = $row['date_delivery_out'];//วันที่ออกส่งสินค้า
          $time_delivery_out  = $row['time_delivery_out'];//เวลาออกส่งสินค้า
          $fuel_cost          = $row['fuel_cost'];//ค่าน้ำมัน
          $fuel_litre         = $row['fuel_litre'];//จำนวนลิตร
          $blank_charge       = $row['blank_charge'];//ค่าตีเปล่า
          $allowance          = $row['allowance'];//เบี้ยเลี้ยง
          $kog_expense        = $row['kog_expense'];//หางคอก
          $allowance_oth      = $row['allowance_oth'];//เบี้ยงเลี้ยงอื่นๆ
          $remark             = $row['remark'];
          $shipment           = $row['shipment'];
          $price_type_id      = $row['price_type_id'];//รหัสการคำนวณ
          $one_trip_ton       = $row['one_trip_ton'];//ราคาต่อตัน
          $ext_one_trip_ton   = $row['ext_one_trip_ton'];//ราคารถร่วมต่อตัน
          $price_per_trip     = $row['price_per_trip'];//ราคาต่อเที่ยว
          $ext_price_per_trip = $row['ext_price_per_trip'];//ราคารถร่วมต่อเที่ยว
          $total_amount_receive = $row['total_amount_receive'];//ราคาค่าขนส่ง
          $total_amount_ap_pay  = $row['total_amount_ap_pay'];//ราคาจ่ายรถร่วม
          $total_amount_allowance   = $row['total_amount_allowance'];//รวมค่าใช้จ่าย
          $job_ended_clearance      = $row['job_ended_clearance'];//ค่าเคลียร์ค่าปรับ
          $job_ended_recap          = $row['job_ended_recap'];//ค่าปะยาง
          $job_ended_expressway     = $row['job_ended_expressway'];//ค่าทางด่วน
          $job_ended_passage_fee    = $row['job_ended_passage_fee'];//ค่าธรรมเนียมผ่านท่าเรือ
          $job_ended_repaires       = $row['job_ended_repaires'];//ค่าซ่อม
          $job_ended_acc_expense    = $row['job_ended_acc_expense'];//ค่าทำบัญชี
          $job_ended_other_expense  = $row['job_ended_other_expense'];//คชจ.อื่นๆ
          $fuel_driver_bill         = $row['fuel_driver_bill'];//บิลน้ำมันจากคนขับ
          $job_status_id            = $row['job_status_id'];

          $color = "";
          if($affiliation_id == 1){
            $color = 'red';
          }else if($affiliation_id == 2){
            $color = 'yellow';
          }else if($affiliation_id == 3){
            $color = 'green';
          }
            $typeAffiliation = '<span class="fa fa-circle" style="color:'.$color.';font-size:20px;"></span>';

          $checked = "";
          if($job_status_id == "2"){
            $checked = "checked";
          }

          $transfer = ($fuel_cost + $allowance + $kog_expense + $blank_charge + $allowance_oth);
          $totalTransfer +=  $transfer;
          $fuel_litres      += $fuel_litre;
          $fuel_costs       += $fuel_cost;
          $allowances       += $allowance;
          $kog_expenses     += $kog_expense;
          $blank_charges    += $blank_charge;
          $allowance_oths   += $allowance_oth;

          $j++;

      ?>
        <tr>
          <td align="center"><?= $j ?></td>
          <td align="left" ><?= $Employee_Name ?></td>
          <td align="right" ><?= $license_plate ?></td>
          <td align="right" ><?= $distance ?></td>
          <td align="right" ><?= number_format($fuel_litre,2); ?></td>
          <td align="right" ><?= number_format($fuel_cost,2);  ?></td>
          <td align="right" ><?= number_format($allowance,2); ?></td>
          <td align="right" ><?= number_format($kog_expense,2); ?></td>
          <td align="right" ><?= number_format($blank_charge,2); ?></td>
          <td align="right" ><?= number_format($allowance_oth,2); ?></td>
          <td align="right" ><?= number_format($transfer,2); ?></td>
          <td align="left" ><?= $cust_dp ?></td>
          <td align="left" ><?= $source ?></td>
          <td align="left" ><?= $destination ?></td>
          <td align="left" ><?= $remark ?></td>
        </tr>
        <?php
          if($employee_id2 != "" && $employee_id2 != "0")
          {
            //$employee_id2       = $row['employee_id2'];//รหัสพนักงาน
            //$second_emp         = $row['$second_emp'];//รหัสพนักงาน
            $allowance_sec       = ($second_emp + $allowance_oth);
            $totalTransfer      +=  $allowance_sec;
            $j++;
          ?>
          <tr>
            <td align="center"><?= $j ?></td>
            <td align="left" ><?= getEmpName($employee_id2);?></td>
            <td align="right" ><?= $license_plate ?></td>
            <td align="right" ><?= $distance ?></td>
            <td align="right" >0.00</td>
            <td align="right" >0.00</td>
            <td align="right" ><?= number_format($second_emp,2); ?></td>
            <td align="right" >0.00</td>
            <td align="right" >0.00</td>
            <td align="right" ><?= number_format($allowance_oth,2); ?></td>
            <td align="right" ><?= number_format($allowance_sec,2); ?></td>
            <td align="left" ><?= $cust_dp ?></td>
            <td align="left" ><?= $source ?></td>
            <td align="left" ><?= $destination ?></td>
            <td align="left" ><?= $remark ?></td>
          </tr>
          <?php
          }
        }
        $totalnet = ($totalTransfer - $fuel_costs);
      ?>
      <tr>
        <td colspan="4" class="text-right" style="border: 1px solid #000000;"><b>ยอดรวม</b></td>
        <td align="right" ><b><?= number_format($fuel_litres,2); ?></b></td>
        <td align="right" ><b><?= number_format($fuel_costs,2); ?></b></td>
        <td align="right" ><b><?= number_format($allowances,2); ?></b></td>
        <td align="right" ><b><?= number_format($kog_expenses,2); ?></b></td>
        <td align="right" ><b><?= number_format($blank_charges,2); ?></b></td>
        <td align="right" ><b><?= number_format($allowance_oths,2); ?></b></td>
        <td align="right" ><b><?= number_format($totalTransfer,2); ?></b></td>
        <td colspan="4" align="left"></td>
      </tr>
      <tr>
        <td colspan="10" align="right"><b>หักค่าน้ำมัน</b></td>
        <td align="right"><b>-<?= number_format($fuel_costs,2); ?></b></td>
        <td colspan="4" align="left"></td>
      </tr>
      <tr>
        <td colspan="10" align="right"><b>รวมเงินที่ต้องโอนทั้งสิ้น</b></td>
        <td align="right"><b><?= number_format($totalnet,2); ?></b></td>
        <td colspan="4" align="left"><b><?= convert(number_format($totalnet,2));?></b></td>
      </tr>
      <tr>
        <td colspan="2" align="center">
          <p>ผู้จัดทำ</p><br>
          <p>...................................................</p>
          <p>(.........../.........../...........)</p>
        </td>
        <td colspan="3" align="center">
         <p>ผู้ตรวจสอบคนที่ 1</p><br>
         <p>...................................................</p>
         <p>(.........../.........../...........)</p>
        </td>
        <td colspan="4" align="center">
         <p>ผู้ตรวจสอบคนที่ 2</p><br>
         <p>...................................................</p>
         <p>(.........../.........../...........)</p>
        </td>
        <td colspan="3" align="center">
         <p>ผู้อนุมัติ</p><br>
         <p>...................................................</p>
         <p>(.........../.........../...........)</p>
        </td>
        <td colspan="2" align="center">
         <p>ผู้โอนเงิน</p><br>
         <p>...................................................</p>
         <p>(.........../.........../...........)</p>
        </td>
        <td align="center">
         <p>บัญชี ตรวจสอบ</p><br>
         <p>...................................................</p>
         <p>(.........../.........../...........)</p>
        </td>
      </tbody>
      </table>
</div>
<script>
window.onbeforeunload = function(){return false;};
setTimeout(function(){window.close();}, 10000);
</script>
</body>
</html>
